## Use ```data_divided_v1```

The datasets are in json format. New divided dataset with the cases where the source is not the author will be added later.

## In ```data_divided_v1```, it contains:
 
99% 7133/7234 of total sentences with sentiment

sarcasm for belief
100% 28/28
sarcasm for sentiment
97% 251/260

## Detail counts in data_divided_v1:

news polarity
none    5650
neg      264
pos      104

posts polarity:
none    20658
neg      4827
pos      1938

posts sarcasm_sentiment 
no     27175
yes      248

news sarcasm_sentiment
no     6015
yes       3

posts sarcasm_belief
no       5231
yes        27

news sarcasm_belief
no      2124
yes        1


## Attributes in the dataset


```
belief_type                                                                none
some possible values: 'cb','na','none','rcb'... 
'none' means there is no any belief type annotated towards to the target


entid_emoffsets               {u'ent-60': [293, 319], u'ent-8': [335, 272], ...
a dict of entity_id and corresponding offsets of entity mentions in the sentence


file_name                                                 AFP_ENG_20100414.0615
file name based on annotation files excluding '.xml'


mtype                                                                       PER
mention type


not_target_inds                                               [5, 6, 9, 12, 16]
the indexes of mentions in the sentence that are not the target 


original_sentence             A former militant of the French far-left group...
original sentence which contains the target tokenized by nltk packages


p_emwinds                     {u'ent-60': [5, 9], u'ent-8': [12, 1], u'ent-3...
a dict of entity ids and a list of indexs of corresponding entity mentions in the sentence 


parent_id                                                                 ent-8
entity/event/relation id of the target 


parent_type                                                              entity
type of target: entity, event, relation


polarity                                                                   none
possible values: none, pos, neg


sarcasm_belief                                                             none
sarcasm label for belief part


sarcasm_sentiment                                                            no
sarcasm label for sentiment part


sentence_targets_indicator    MENTION_INDICATOR_A former militant of MENTION...
the original sentence with mention indicator 'MENTION_INDICATOR' added


small_sentence_w_target        A former militant of the French far-left grou...
smaller sentence with target by getting rid of some unrelated chunks


source_id_belief                                                           None
source_id_sentiment                                                        None
source id for sentiment/belief label


source_is_author_belief                                                       2
source_is_author_sentiment                                                    2
if the source is the author: 1 YES, 0 NO, 2 for newswire data since it does not have the author


source_offset_belief                                                        NaN
source_offset_sentiment                                                     271
source_text_belief                                                         None
source_text_sentiment                                                      None


splited_s_ranges                                                      [[1, 30]]
a list of word index ranges of small chunks in the original sentence


subtype                                                                     NOM
subtype of the target


target_id                                                                  m-42


target_index                                                                  1
target index in the original sentence


target_is_author_mention                                                      0
if the target is the author mention: 1 YES, 0 NO


target_offset                                                               272


target_ranges                                                         [[1, 30]]
small index ranges that contain the target


target_text                   A former militant of the French far-left group...
text of the target


tokenized_sentence            [A, former, militant, of, the, French, far-lef...
a list of tokenized words in the original sentence using stanford tokenizer


words_in_target_ranges        [[A, former, militant, of, the, French, far-le...
a list of lists of tokenized words in target_ranges of the small sentence

```

