# -*- coding: utf-8 -*-
'''
    Module used to calculate scores for all files (ere.xml, best.xml, predicted.xml)
    in a folder, and get averaged results.
    '''
import os
import sys
from os import listdir
from os.path import isfile, join
import subprocess
from deft_ere import RelationMention, read_ere_xml
from deft_best import read_best_xml, read_source
import numpy as np

def cal_stat(results):
    stats = []
    print 'len of results is: ', len(results)
    for res in results:
        if len(res) == 0:
            print 'len(res) is 0-----------!-------#'
        else:
            perentage = res.count(True) / float(len(res))
            stats.append(perentage)

    return stats

def res_handler(ere_path, gold_path, src_path):
    file_names = [f for f in listdir(src_path) if isfile(join(src_path, f))]
    # print file_names
    post_file_names = [f for f in file_names if f.endswith('cmp.txt')]
    # print post_file_names
#     total_st_p_bf = []
#     total_st_q_bf = []
#     total_st_auh_p_bf = []
#     total_st_auh_q_bf = []
#     total_tag_in_q_bf = []
#     total_src_in_q_bf = []
#     total_tag_in_p_bf = []
#     total_src_in_p_bf = []
#     total_tag_is_q_bf = []
#     total_tag_is_p_bf = []
#     total_src_is_q_bf = []
#     total_src_is_p_bf = []
#     total_tag_type_bf = []
#     total_tag_mtype_bf = []
#
#     for i in xrange(1, len(post_file_names)):
#         file_name = post_file_names[i]
#         base_name = file_name.replace('.cmp.txt','')
#         ere_fn = '{0}.rich_ere.xml'.format(base_name)
#         ere_file = join(ere_path, ere_fn)
#         src_file = join(src_path, file_name)
#         best_fn = '{0}.best.xml'.format(base_name)
#         best_file = join(gold_path, best_fn)
#         ere_annotations = read_ere_xml(ere_file)
#         src_range = read_source(src_file)
#         best_annotations = read_best_xml(ere_annotations, src_range, best_file)
#
#         src_tag_p = [belief.src_tag_p for belief in best_annotations.beliefs]
#         src_tag_q = [belief.src_tag_q for belief in best_annotations.beliefs]
#         tag_in_q = [belief.tag_in_quote for belief in best_annotations.beliefs]
#         src_in_q = [belief.src_in_quote for belief in best_annotations.beliefs]
#         src_auh_tag_p = [belief.src_auh_tag_p for belief in best_annotations.beliefs]
#         src_auh_tag_q = [belief.src_auh_tag_q for belief in best_annotations.beliefs]
#         src_is_quoter = [belief.src_is_quoter for belief in best_annotations.beliefs]
#         src_is_poster = [belief.src_is_poster for belief in best_annotations.beliefs]
#         src_in_post = [belief.src_in_post for belief in best_annotations.beliefs]
#         tag_in_post = [belief.tag_in_post for belief in best_annotations.beliefs]
#         tag_is_quoter = [belief.tag_is_quoter for belief in best_annotations.beliefs]
#         tag_is_poster = [belief.tag_is_poster for belief in best_annotations.beliefs]
#         tag_type = [belief.tag_type for belief in best_annotations.beliefs]
#         tag_mtype = [belief.tag_mtype for belief in best_annotations.beliefs]
#
#         # src_in_q_src = [(belief.source.mention_id, belief.source.offset) for belief in best_annotations.sentiments if belief.src_in_quote]
#         # if src_in_q_src:
#         #     print "The name of the file that contains src_in_quote is: {0} ------------------".format(base_name)
#         #     print "the mention_id and offset of these sources are: {1} ------------------ ".format(src_in_q_src)
#
#         src_in_q_src_bf = [(belief.source.mention_id, belief.source.offset) for belief in best_annotations.beliefs if belief.src_in_quote]
#         if src_in_q_src_bf:
#             print "The name of the file that contains some sources in quotes is: {0} ------------------".format(base_name)
#             print "the mention_id and offset of these sources are: {0} ------------------ ".format(src_in_q_src_bf)
#
#         src_in_p_src_bf = [(belief.source.mention_id, belief.source.offset) for belief in best_annotations.beliefs if belief.src_in_post]
#         if src_in_p_src_bf:
#             print "The name of the file that contains some sources in posts is: {0} ------------------".format(base_name)
#             print "the mention_id and offset of these sources are: {0} ------------------ ".format(src_in_p_src_bf)
#
#
#         total_st_p_bf.extend(src_tag_p)
#         total_st_q_bf.extend(src_tag_q)
#         total_tag_in_q_bf.extend(tag_in_q)
#         total_src_in_q_bf.extend(src_in_q)
#         total_st_auh_p_bf.extend(src_auh_tag_p)
#         total_st_auh_q_bf.extend(src_auh_tag_q)
#         total_tag_in_p_bf.extend(tag_in_post)
#         total_src_in_p_bf.extend(src_in_post)
#         total_tag_is_q_bf.extend(tag_is_quoter)
#         total_tag_is_p_bf.extend(tag_is_poster)
#         total_src_is_q_bf.extend(src_is_quoter)
#         total_src_is_p_bf.extend(src_is_poster)
#         total_tag_type_bf.extend(tag_type)
#         total_tag_mtype_bf.extend(tag_mtype)
#
#     results = [total_st_p_bf,total_st_q_bf,total_st_auh_p_bf, total_st_auh_q_bf, total_tag_in_q_bf, total_src_in_q_bf, total_tag_in_p_bf, total_src_in_p_bf, total_tag_is_q_bf, total_tag_is_p_bf, total_src_is_q_bf, total_src_is_p_bf]
#     stats = cal_stat(results)
#     print '#############RESULTS FOR BELIEFS################\n'
#     print '-------for src and tag--------\n'
#     print 'st_quote is {0}, st_post is {1}, st_auh_q is {2}, st_auh_p is {3}, \n'.format(stats[1], stats[0], stats[3], stats[2])
#     print 'src (q/per) and tag are in same post/quote is {0}\n'.format(stats[3]+stats[2])
#     print 'src (no matter q/per or not) and tag are in same p/q is {0}\n'.format(stats[1]+stats[0])
#     print 'src (not q/per) and tag are in same post/quote is {0}\n'.format(stats[1]+stats[0]-stats[3]-stats[2])
#     print 'src (q/per) and tag are not in same post/quote is {0}\n'.format(stats[10]+stats[11]-stats[3]-stats[2])
#     print 'src (no matter q/per or not) and tag are not in same p/q is {0}\n'.format(1-stats[1]-stats[0])
#     print 'src (not q/per) and tag are not in same p/q is {0}\n'.format(1-(stats[3]+stats[2])-(stats[10]+stats[11]-stats[3]-stats[2]) -(stats[1]+stats[0]-stats[3]-stats[2]))
#     print '-------for src only--------\n'
#     print 'src_is_q is {0}, src_is_p is {1}, src_in_q is {2}, src_in_p is {3}\n'.format(stats[10], stats[11], stats[5], stats[7])
#     print 'src is a quoter or poster: {0}\n'.format(stats[10]+stats[11])
#     print 'src is in p/q: {0}\n'.format(stats[5]+stats[7])
#     print '-------for tag only--------\n'
#     print 'tag_is_q is {0}, tag_is_p is {1}, tag_in_q is {2}, tag_in_p is {3}\n'.format(stats[8], stats[9], stats[4], stats[6])
#     # total_tag_type_bf.index(0)
# # total_
#
#     total_st_p_bf = []
#     total_st_q_bf = []
#     total_st_auh_p_bf = []
#     total_st_auh_q_bf = []
#     total_tag_in_q_bf = []
#     total_src_in_q_bf = []
#     total_tag_in_p_bf = []
#     total_src_in_p_bf = []
#     total_tag_is_q_bf = []
#     total_tag_is_p_bf = []
#     total_src_is_q_bf = []
#     total_src_is_p_bf = []
#     total_tag_type_bf = []
#     total_tag_mtype_bf = []
#
#     for i in xrange(1, len(post_file_names)):
#         file_name = post_file_names[i]
#         base_name = file_name.replace('.cmp.txt','')
#         ere_fn = '{0}.rich_ere.xml'.format(base_name)
#         ere_file = join(ere_path, ere_fn)
#         src_file = join(src_path, file_name)
#         best_fn = '{0}.best.xml'.format(base_name)
#         best_file = join(gold_path, best_fn)
#         ere_annotations = read_ere_xml(ere_file)
#         src_range = read_source(src_file)
#         best_annotations = read_best_xml(ere_annotations, src_range, best_file)
#
#         src_tag_p = [belief.src_tag_p for belief in best_annotations.argument_beliefs]
#         src_tag_q = [belief.src_tag_q for belief in best_annotations.argument_beliefs]
#         tag_in_q = [belief.tag_in_quote for belief in best_annotations.argument_beliefs]
#         src_in_q = [belief.src_in_quote for belief in best_annotations.argument_beliefs]
#         src_auh_tag_p = [belief.src_auh_tag_p for belief in best_annotations.argument_beliefs]
#         src_auh_tag_q = [belief.src_auh_tag_q for belief in best_annotations.argument_beliefs]
#         src_is_quoter = [belief.src_is_quoter for belief in best_annotations.argument_beliefs]
#         src_is_poster = [belief.src_is_poster for belief in best_annotations.argument_beliefs]
#         src_in_post = [belief.src_in_post for belief in best_annotations.argument_beliefs]
#         tag_in_post = [belief.tag_in_post for belief in best_annotations.argument_beliefs]
#         tag_is_quoter = [belief.tag_is_quoter for belief in best_annotations.argument_beliefs]
#         tag_is_poster = [belief.tag_is_poster for belief in best_annotations.argument_beliefs]
#         # tag_type = [belief.tag_type for belief in best_annotations.argument_beliefs]
#         # tag_mtype = [belief.tag_mtype for belief in best_annotations.argument_beliefs]
#
#         # src_in_p_src_arg = [(belief.source.mention_id, belief.source.offset) for belief in best_annotations.argument_beliefs if belief.src_in_post]
#         # if src_in_p_src_arg:
#         #     print "The name of the file that contains src_in_quote is: {0} ------------------".format(base_name)
#         #     print "the mention_id and offset of these sources are: {1} ------------------ ".format(src_in_p_src_arg)
#
#         total_st_p_bf.extend(src_tag_p)
#         total_st_q_bf.extend(src_tag_q)
#         total_tag_in_q_bf.extend(tag_in_q)
#         total_src_in_q_bf.extend(src_in_q)
#         total_st_auh_p_bf.extend(src_auh_tag_p)
#         total_st_auh_q_bf.extend(src_auh_tag_q)
#         total_tag_in_p_bf.extend(tag_in_post)
#         total_src_in_p_bf.extend(src_in_post)
#         total_tag_is_q_bf.extend(tag_is_quoter)
#         total_tag_is_p_bf.extend(tag_is_poster)
#         total_src_is_q_bf.extend(src_is_quoter)
#         total_src_is_p_bf.extend(src_is_poster)
#     # total_tag_type_bf.extend(tag_type)
#     # total_tag_mtype_bf.extend(tag_mtype)
#
#     results = [total_st_p_bf,total_st_q_bf,total_st_auh_p_bf, total_st_auh_q_bf, total_tag_in_q_bf, total_src_in_q_bf, total_tag_in_p_bf, total_src_in_p_bf, total_tag_is_q_bf, total_tag_is_p_bf, total_src_is_q_bf, total_src_is_p_bf]
#     stats = cal_stat(results)
#     print '#############RESULTS FOR ARG BELIEFS################\n'
#     print '-------for src and tag--------\n'
#     print 'st_quote is {0}, st_post is {1}, st_auh_q is {2}, st_auh_p is {3}, \n'.format(stats[1], stats[0], stats[3], stats[2])
#     print 'src (q/per) and tag are in same post/quote is {0}\n'.format(stats[3]+stats[2])
#     print 'src (no matter q/per or not) and tag are in same p/q is {0}\n'.format(stats[1]+stats[0])
#     print 'src (not q/per) and tag are in same post/quote is {0}\n'.format(stats[1]+stats[0]-stats[3]-stats[2])
#     print 'src (q/per) and tag are not in same post/quote is {0}\n'.format(stats[10]+stats[11]-stats[3]-stats[2])
#     print 'src (no matter q/per or not) and tag are not in same p/q is {0}\n'.format(1-stats[1]-stats[0])
#     print 'src (not q/per) and tag are not in same p/q is {0}\n'.format(1-(stats[3]+stats[2])-(stats[10]+stats[11]-stats[3]-stats[2]) -(stats[1]+stats[0]-stats[3]-stats[2]))
#     print '-------for src only--------\n'
#     print 'src_is_q is {0}, src_is_p is {1}, src_in_q is {2}, src_in_p is {3}\n'.format(stats[10], stats[11], stats[5], stats[7])
#     print 'src is a quoter or poster: {0}\n'.format(stats[10]+stats[11])
#     print 'src is in p/q: {0}\n'.format(stats[5]+stats[7])
#     print '-------for tag only--------\n'
#     print 'tag_is_q is {0}, tag_is_p is {1}, tag_in_q is {2}, tag_in_p is {3}\n'.format(stats[8], stats[9], stats[4], stats[6])

    total_st_p_bf = []
    total_st_q_bf = []
    total_st_auh_p_bf = []
    total_st_auh_q_bf = []
    total_tag_in_q_bf = []
    total_src_in_q_bf = []
    total_tag_in_p_bf = []
    total_src_in_p_bf = []
    total_tag_is_q_bf = []
    total_tag_is_p_bf = []
    total_src_is_q_bf = []
    total_src_is_p_bf = []
    total_tag_type_bf = []
    total_tag_mtype_bf = []

    for i in xrange(1, len(post_file_names)):
        file_name = post_file_names[i]
        base_name = file_name.replace('.cmp.txt','')
        ere_fn = '{0}.rich_ere.xml'.format(base_name)
        ere_file = join(ere_path, ere_fn)
        src_file = join(src_path, file_name)
        best_fn = '{0}.best.xml'.format(base_name)
        best_file = join(gold_path, best_fn)
        ere_annotations = read_ere_xml(ere_file)
        src_range = read_source(src_file)
        best_annotations = read_best_xml(ere_annotations, src_range, best_file)
        print 'best_annotations.sentiments is: ', best_annotations.beliefs
        src_tag_p = [belief.src_tag_p for belief in best_annotations.sentiments]
        src_tag_q = [belief.src_tag_q for belief in best_annotations.sentiments]
        tag_in_q = [belief.tag_in_quote for belief in best_annotations.sentiments]
        src_in_q = [belief.src_in_quote for belief in best_annotations.sentiments]
        src_auh_tag_p = [belief.src_auh_tag_p for belief in best_annotations.sentiments]
        src_auh_tag_q = [belief.src_auh_tag_q for belief in best_annotations.sentiments]
        src_is_quoter = [belief.src_is_quoter for belief in best_annotations.sentiments]
        src_is_poster = [belief.src_is_poster for belief in best_annotations.sentiments]
        src_in_post = [belief.src_in_post for belief in best_annotations.sentiments]
        tag_in_post = [belief.tag_in_post for belief in best_annotations.sentiments]
        tag_is_quoter = [belief.tag_is_quoter for belief in best_annotations.sentiments]
        tag_is_poster = [belief.tag_is_poster for belief in best_annotations.sentiments]
        tag_type = [belief.tag_type for belief in best_annotations.sentiments]
        tag_mtype = [belief.tag_mtype for belief in best_annotations.sentiments]

        src_in_q_src = [(belief.source.mention_id, belief.source.offset) for belief in best_annotations.sentiments if belief.src_in_quote]
        if src_in_q_src:
            print "The name of the file that contains some sources in quotes is: {0} ------------------".format(base_name)
            print "the mention_id and offset of these sources are: {0} ------------------ ".format(src_in_q_src)
        src_in_p_src = [(belief.source.mention_id, belief.source.offset) for belief in best_annotations.sentiments if belief.src_in_post]
        if src_in_p_src:
            print "The name of the file that contains some sources in posts is: {0} ------------------".format(base_name)
            print "the mention_id and offset of these sources are: {0} ------------------ ".format(src_in_p_src)

        total_st_p_bf.extend(src_tag_p)
        total_st_q_bf.extend(src_tag_q)
        total_tag_in_q_bf.extend(tag_in_q)
        total_src_in_q_bf.extend(src_in_q)
        total_st_auh_p_bf.extend(src_auh_tag_p)
        total_st_auh_q_bf.extend(src_auh_tag_q)
        total_tag_in_p_bf.extend(tag_in_post)
        total_src_in_p_bf.extend(src_in_post)
        total_tag_is_q_bf.extend(tag_is_quoter)
        total_tag_is_p_bf.extend(tag_is_poster)
        total_src_is_q_bf.extend(src_is_quoter)
        total_src_is_p_bf.extend(src_is_poster)
        total_tag_type_bf.extend(tag_type)
        total_tag_mtype_bf.extend(tag_mtype)
    print total_st_p_bf
    results = [total_st_p_bf,total_st_q_bf,total_st_auh_p_bf, total_st_auh_q_bf, total_tag_in_q_bf, total_src_in_q_bf, total_tag_in_p_bf, total_src_in_p_bf, total_tag_is_q_bf, total_tag_is_p_bf, total_src_is_q_bf, total_src_is_p_bf]
    stats = cal_stat(results)
    print '#############RESULTS FOR SENTIMENTS################\n'
    print '-------for src and tag--------\n'
    print 'st_quote is {0}, st_post is {1}, st_auh_q is {2}, st_auh_p is {3}, \n'.format(stats[1], stats[0], stats[3], stats[2])
    print 'src (q/per) and tag are in same post/quote is {0}\n'.format(stats[3]+stats[2])
    print 'src (no matter q/per or not) and tag are in same p/q is {0}\n'.format(stats[1]+stats[0])
    print 'src (not q/per) and tag are in same post/quote is {0}\n'.format(stats[1]+stats[0]-stats[3]-stats[2])
    print 'src (q/per) and tag are not in same post/quote is {0}\n'.format(stats[10]+stats[11]-stats[3]-stats[2])
    print 'src (no matter q/per or not) and tag are not in same p/q is {0}\n'.format(1-stats[1]-stats[0])
    print 'src (not q/per) and tag are not in same p/q is {0}\n'.format(1-(stats[3]+stats[2])-(stats[10]+stats[11]-stats[3]-stats[2]) -(stats[1]+stats[0]-stats[3]-stats[2]))
    print '-------for src only--------\n'
    print 'src_is_q is {0}, src_is_p is {1}, src_in_q is {2}, src_in_p is {3}\n'.format(stats[10], stats[11], stats[5], stats[7])
    print 'src is a quoter or poster: {0}\n'.format(stats[10]+stats[11])
    print 'src is in p/q: {0}\n'.format(stats[5]+stats[7])
    print '-------for tag only--------\n'
    print 'tag_is_q is {0}, tag_is_p is {1}, tag_in_q is {2}, tag_in_p is {3}\n'.format(stats[8], stats[9], stats[4], stats[6])


def avg_srctag_rate(ere_path, gold_path, src_path):
    res_handler(ere_path, gold_path, src_path)
# file_names = [f for f in listdir(gold_path) if isfile(join(gold_path, f))]
# srctag_rates_q = []
# srctag_rates_p = []
#
# # stats_belief =
# total_st_p_belief = []
# total_st_q_belief = []
# total_tag_in_q_belief = []
# total_src_in_q_belief = []
# total_st_auh_p_bf = []
# total_st_auh_q_bf = []
# total_tag_in_p_bf = []
# total_src_in_p_bf = []
# total_tag_is_q_bf = []
# total_tag_is_p_bf = []
# total_src_is_q_bf = []
# total_src_is_p_bf = []
# total_tag_type = []
# total_tag_mtype = []
#
# total_st_p_sent = []
# total_st_q_sent = []
# total_tag_in_q_sent = []
# total_src_in_q_sent = []
# total_st_auh_p_bf = []
# total_st_auh_q_bf = []
# total_tag_in_p_bf = []
# total_src_in_post = []
# total_tag_is_q = []
# total_tag_is_p = []
# total_src_is_q = []
# total_src_is_p = []
# total_tag_type = []
# total_tag_mtype = []
#
# total_st_p_argbelief = []
# total_st_q_argbelief = []
# total_tag_in_q_argbelief = []
# total_src_in_q_argbelief = []
#
# for i in xrange(1, len(file_names)):
#     print i
#     file_name = file_names[i]
#     base_name = file_name.replace('.best.xml','')
#     print "file name of this iter is: ", base_name
#     ere_fn = '{0}.rich_ere.xml'.format(base_name)
#     ere_file = join(ere_path, ere_fn)
#     best_file = join(gold_path, file_name)
#     src_fn = '{0}.cmp.txt'.format(base_name)
#     src_file = join(src_path, src_fn)
#     ere_annotations = read_ere_xml(ere_file)
#     src_range = read_source(src_file)
#     best_annotations = read_best_xml(ere_annotations, src_range, best_file)
#
#     belief_src_tag_p = [belief.src_tag_p for belief in best_annotations.beliefs]
#     belief_src_tag_q = [belief.src_tag_q for belief in best_annotations.beliefs]
#     belief_tag_in_q = [belief.tag_in_quote for belief in best_annotations.beliefs]
#     belief_src_in_q = [belief.src_in_quote for belief in best_annotations.beliefs]
#     total_st_p_belief.extend(belief_src_tag_p)
#     total_st_q_belief.extend(belief_src_tag_q)
#     total_tag_in_q_belief.extend(belief_tag_in_q)
#     total_src_in_q_belief.extend(belief_src_in_q)
#     # print belief_src_tag_q
#
#     if belief_src_tag_p:
#         # print belief_src_tag_p
#         p_belief = belief_src_tag_p.count(True) / float(len(belief_src_tag_p))
#     else:
#         p_belief = np.nan
#     if belief_src_tag_q:
#         # print belief_src_tag_p
#         q_belief = belief_src_tag_q.count(True) / float(len(belief_src_tag_q))
#     else:
#         q_belief = np.nan
#
#     sentiment_src_tag_p = [sentiment.src_tag_p for sentiment in best_annotations.sentiments]
#     sentiment_src_tag_q = [sentiment.src_tag_q for sentiment in best_annotations.sentiments]
#     sentiment_tag_in_q = [sentiment.tag_in_quote for sentiment in best_annotations.sentiments]
#     sentiment_src_in_q = [sentiment.src_in_quote for sentiment in best_annotations.sentiments]
#     total_st_p_sent.extend(sentiment_src_tag_p)
#     total_st_q_sent.extend(sentiment_src_tag_q)
#     total_tag_in_q_sent.extend(sentiment_tag_in_q)
#     total_src_in_q_sent.extend(sentiment_src_in_q)
#
#     if sentiment_src_tag_p:
#         # print sentiment_src_tag
#         p_sentiment = sentiment_src_tag_p.count(True) / float(len(sentiment_src_tag_p))
#     else:
#         p_sentiment = np.nan
#     if sentiment_src_tag_q:
#         # print sentiment_src_tag
#         q_sentiment = sentiment_src_tag_q.count(True) / float(len(sentiment_src_tag_q))
#     else:
#         q_sentiment = np.nan
#
#     arg_belief_src_tag_p = [belief.src_tag_p for belief in best_annotations.argument_beliefs]
#     arg_belief_src_tag_q = [belief.src_tag_q for belief in best_annotations.argument_beliefs]
#     arg_belief_tag_in_q = [belief.tag_in_quote for belief in best_annotations.argument_beliefs]
#     arg_belief_src_in_q = [belief.src_in_quote for belief in best_annotations.argument_beliefs]
#     total_st_p_argbelief.extend(arg_belief_src_tag_p)
#     total_st_q_argbelief.extend(arg_belief_src_tag_q)
#     total_tag_in_q_argbelief.extend(arg_belief_tag_in_q)
#     total_src_in_q_argbelief.extend(arg_belief_src_in_q)
#
#     if arg_belief_src_tag_p:
#         # print arg_belief_src_tag_p
#         p_arg_belief = arg_belief_src_tag_p.count(True) / float(len(arg_belief_src_tag_p))
#     else:
#         p_arg_belief = np.nan
#     if arg_belief_src_tag_q:
#         # print arg_belief_src_tag_p
#         q_arg_belief = arg_belief_src_tag_q.count(True) / float(len(arg_belief_src_tag_q))
#     else:
#         q_arg_belief = np.nan
#
#     srctag_rates_q.append((q_belief, q_sentiment, q_arg_belief))
#     srctag_rates_p.append((p_belief, p_sentiment, p_arg_belief))
#
#     print '\nthe avg srctag_p rates of {0} are: \n p_belief: {1}, p_sentiment: {2}, p_arg_belief: {3}'.format(base_name, p_belief, p_sentiment, p_arg_belief)
#     print '\nthe avg srctag_q rates of {0} are: \n q_belief: {1}, q_sentiment: {2}, q_arg_belief: {3}'.format(base_name, q_belief, q_sentiment, q_arg_belief)
#     print '------------------------------------------------------------------------------------'
# avg_srctag_rate_q = np.nanmean(srctag_rates_q, axis=0) #tuple(mean(srctag_rates, axis=0))
# avg_srctag_rate_p = np.nanmean(srctag_rates_p, axis=0)
#
# print '#############AVERAGE RESULTS OF ALL FILES##############\n'
# print 'the avg srctag_p rate for whole files by avging the result of each file is: {0}\n'.format(avg_srctag_rate_p)
# print 'the avg srctag_q rate for whole files by avging the result of each file is: {0}\n'.format(avg_srctag_rate_q)
#
# print '#############AVERAGE RESULTS OF ALL BELIEFS################\n'
# print 'total number of beliefs is: ', len(total_st_p_belief)
# print '\ntotal number of sentiments is: ', len(total_st_p_sent)
# print '\ntotal number of arg_beliefs is: ', len(total_st_p_argbelief)
#
#
# results_belief = [total_st_p_belief,total_st_q_belief,total_tag_in_q_belief, total_src_in_q_belief]
# stat_belief = cal_stat(results_belief)
# print "\nThe stats of avg perentage for beliefs: \nst_post is {0}, st_quote is {1}, tag_in_q is {2}, src_in_q is {3}\n".format(stat_belief[0], stat_belief[1], stat_belief[2], stat_belief[3])
# results_sent = [total_st_p_sent, total_st_q_sent, total_tag_in_q_sent, total_src_in_q_sent]
# stat_sentiment = cal_stat(results_sent)
# print "The stats of avg perentage for sentiments: \nst_post is {0}, st_quote is {1}, tag_in_q is {2}, src_in_q is {3}\n".format(stat_sentiment[0], stat_sentiment[1], stat_sentiment[2], stat_sentiment[3])
# results_argbelief = [total_st_p_argbelief, total_st_q_argbelief, total_tag_in_q_argbelief, total_src_in_q_argbelief]
# stat_argbelief = cal_stat(results_argbelief)
# print "The stats of avg perentage for arg beliefs: \nst_post is {0}, st_quote is {1}, tag_in_q is {2}, src_in_q is {3}\n".format(stat_argbelief[0], stat_argbelief[1], stat_argbelief[2], stat_argbelief[3])
#
# print '##############IF CONSIDER st_post AND st_quote TOGETHER##############\n'
# print "The stats of avg perentage for beliefs: \nrate of src(is an author) and tag are in the same post is {0}".format(stat_belief[0]+stat_belief[1])
# print "The stats of avg perentage for sentiments: \nrate of src(is an author) and tag are in the same post is {0}".format(stat_sentiment[0]+stat_sentiment[1])
# print "The stats of avg perentage for arg beliefs: \nrate of src(is an author) and tag are in the same post is {0}".format(stat_argbelief[0]+stat_argbelief[1])

def avg_evaluator(ere_path, gold_path, sys_path):

    test_file_names = [f.split('.')[0] for f in listdir(sys_path) if isfile(join(sys_path, f))]
    p_values = []
    r_values = []
    f_values = []
    for i in xrange(1, len(test_file_names)):
        ere_file_path = join(ere_path, test_file_names[i]) + '.rich_ere.xml'
        gold_best_path = join(gold_path, test_file_names[i]) + '.best.xml'
        sys_best_path = join(sys_path, test_file_names[i]) + '.best.xml'

        if isfile(ere_file_path) and isfile(gold_best_path) and isfile(sys_best_path):
            #get averaged result
            command = ['python', 'best_evaluator.py', '-s', ere_file_path, gold_best_path, sys_best_path]
            proc = subprocess.Popen(command, stdout=subprocess.PIPE, shell=False)
            (out, err) = proc.communicate()

            print 'The scores for the file {0} are (see below): '.format(test_file_names[i])
            print out
            if not out is None and out != '':
                p_value = float(out[out.index('P')+3 : out.index('R')])
                r_value = float(out[out.index('R')+3 : out.index('F')])
                f_value = float(out[out.index('F')+3 : len(out)-1])
                p_values.append(p_value)
                r_values.append(r_value)
                f_values.append(f_value)

    p_avg = sum(p_values) / float(len(p_values)) if float(len(p_values)) > 0.0 else 1.0
    r_avg = sum(r_values) / float(len(r_values)) if float(len(r_values)) > 0.0 else 1.0
    f_avg = 2 * (p_avg * r_avg) / (p_avg + r_avg) if (p_avg+r_avg) > 0.0 else 0.0

    print '\nThe average scores for the system-outputs are: '
    print(' avg P: {0}  avg R: {1}  avg F: {2}'.format(p_avg, r_avg, f_avg))

if __name__ == '__main__':
    # if len(sys.argv) == 4:
    #     ere_eng_path = sys.argv[1]
    #     best_eng_path = sys.argv[2]
    #     sys_best_path = sys.argv[3]
    #     print('Successfully read file pathes')
    #     avg_evaluator(ere_eng_path, best_eng_path, sys_best_path)
    # else:
    #     print('sys_tester.py -- code to evaluate the average scores of system on BeSt annotations.')
    #     print(' Usage: python sys_tester.py [path to the folder of ere files] [path to the folder of gold best files] [path to the folder of system prodcued best files]')

    #BE CAUTION: read_best_xml MAY NOT WORK FOR DIR WHERE HAS WHITE SPACE FOR FOLDERS OR FILES
    # best_eng_path = '/Users/taoyds/Documents/Research/Data_DEFT/LDC2016E27_DEFT_English_Belief_and_Sentiment_Annotation/data/system-outputs/Marlies-Axinia-Sentiment_20160323_46_src_intra_offset0/annotation'
    # ere_eng_path = '/Users/taoyds/Documents/Research/Data_DEFT/LDC2016E27_DEFT_English_Belief_and_Sentiment_Annotation/data/system-outputs/Marlies-Axinia-Sentiment/20160331/ere'
    # #Sardar-Belief/xml-belief_annotation
    # # sys_best_path = '/Users/taoyds/Documents/Research/Data_DEFT/LDC2016E27_DEFT_English_Belief_and_Sentiment_Annotation/data/system-outputs/Sardar-Belief/xml-belief_annotation'
    # #Noura-Sentiment/most basic_output/xml-files-new
    # # sys_best_path = '/Users/taoyds/Documents/Research/Data_DEFT/LDC2016E27_DEFT_English_Belief_and_Sentiment_Annotation/data/system-outputs/Noura-Sentiment/most basic_output/xml-files-new'
    # #for all
    # sys_best_path = '/Users/taoyds/Documents/Research/Data_DEFT/LDC2016E27_DEFT_English_Belief_and_Sentiment_Annotation/data/system-outputs/Marlies-Axinia-Sentiment/20160331/best_xml'
    # #Marlies-Axinia-Sentiment/20160323
    # sys_best_path = "/Users/taoyds/Documents/Research/Data_DEFT/LDC2016E27_DEFT_English_Belief_and_Sentiment_Annotation/data/system-outputs/Marlies-Axinia-Sentiment/20160323/best_xml"
    # avg_evaluator(ere_eng_path, best_eng_path, sys_best_path)



    # src_eng_path ='/Users/taoyds/Documents/belief_sent/Data_DEFT/best_annotation_eng/data/all_data/source'
    # ere_eng_path ='/Users/taoyds/Documents/belief_sent/Data_DEFT/best_annotation_eng/data/all_data/ere'
    # best_eng_path = '/Users/taoyds/Documents/belief_sent/Data_DEFT/best_annotation_eng/data/all_data/annotation'
    # avg_srctag_rate(ere_eng_path, best_eng_path, src_eng_path)

    src_ch = '/Users/taoyds/Documents/BeSt/Data_DEFT/best_annotation_eng/data/new_dataset/data/source'
    ere_ch = '/Users/taoyds/Documents/BeSt/Data_DEFT/best_annotation_eng/data/new_dataset/data/ere'
    best_ch = '/Users/taoyds/Documents/BeSt/Data_DEFT/best_annotation_eng/data/new_dataset/data/annotation'
    avg_srctag_rate(ere_ch, best_ch, src_ch)
