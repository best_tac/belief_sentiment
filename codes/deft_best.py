"""
This module contains classes to represent belief and sentiment annotations
and methods to read XML annotations into an OOP representation.
"""
import sys
import re
import xml.etree.ElementTree as ET
from xml.etree.ElementTree import tostring
from deft_ere import EREAnnotations, RelationMention, EventMention, AnnotationFormatError, read_ere_xml

__author__ = 'Daniel Bauer'
__email__ = 'bauer@cs.columbia.edu'
__date__ = 'May 31 2016'


class AnnotationConsistencyError(RuntimeError):
    pass

class Belief(object):
    '''
    Meaning of variable names (all values are in percentage):
    st_quote: the source and target are in the same quote (even if source is not an author)
    st_post: the source and target are in the same post (even if source is not an author)
    st_auh_q: the source is an author of a quote, and it is in the same quote as the target
    st_auh_p: the source is an author of a post, and it is in the same post as the target
    src (q/per): the source is an author of either a post or quote
    src_is_q: the source is an author of a quote
    src_is_p: the source is an author of a post
    src_in_q: the source is not an author, but it is in a quote.
    src_in_p: the source is not an author, but it is in a post.
    the same for tag (target)
    '''
    def __init__(self, source_mention, target_mention, btype, polarity, sarcasm, src_auh_tag_p, src_auh_tag_q, tag_in_quote, src_in_quote, src_is_quoter, src_is_poster, src_tag_p, src_tag_q, tag_in_post, src_in_post, tag_is_quoter, tag_is_poster, tag_type, tag_mtype):
        self.source = source_mention
        self.target = target_mention # relation or event mention
        self.belief_type = btype
        self.polarity = polarity
        self.sarcasm = sarcasm
        self.src_auh_tag_p = src_auh_tag_p #true if the source and the target its belief is toward to are in the same post
        self.src_auh_tag_q = src_auh_tag_q #true if the source and the target its belief is toward to are in the same quote
        self.tag_in_quote = tag_in_quote
        self.src_in_quote = src_in_quote
        self.src_is_quoter = src_is_quoter
        self.src_is_poster = src_is_poster
        self.src_tag_p = src_tag_p
        self.src_tag_q = src_tag_q
        self.tag_in_post = tag_in_post
        self.src_in_post = src_in_post
        self.tag_is_quoter = tag_is_quoter
        self.tag_is_poster = tag_is_poster
        self.tag_type = tag_type
        self.tag_mtype = tag_mtype

    def __repr__(self):
        return '<Belief {0} {1} {2}>'.format(self.belief_type, self.source.mention_id, self.target.mention_id,)

class ArgumentBelief(object):
    """
    A belief whose target is an event argument.
    """
    def __init__(self, source_mention, target_event_mention, target_argument, btype, polarity, sarcasm, src_auh_tag_p, src_auh_tag_q, tag_in_quote, src_in_quote, src_is_quoter, src_is_poster, src_tag_p, src_tag_q, tag_in_post, src_in_post, tag_is_quoter, tag_is_poster):

        self.source = source_mention
        self.target_event_mention = target_event_mention
        self.target = target_argument
        self.belief_type = btype
        self.polarity = polarity
        self.sarcasm = sarcasm
        self.src_auh_tag_p = src_auh_tag_p #true if the source and the target its belief is toward to are in the same post
        self.src_auh_tag_q = src_auh_tag_q #true if the source and the target its belief is toward to are in the same quote
        self.tag_in_quote = tag_in_quote
        self.src_in_quote = src_in_quote
        self.src_is_quoter = src_is_quoter
        self.src_is_poster = src_is_poster
        self.src_tag_p = src_tag_p
        self.src_tag_q = src_tag_q
        self.tag_in_post = tag_in_post
        self.src_in_post = src_in_post
        self.tag_is_quoter = tag_is_quoter
        self.tag_is_poster = tag_is_poster

    def __repr__(self):
        return '<ArgumentBelief {0} {1} {3} (of {2})>'.format(self.belief_type, self.source.mention_id, self.target_event_mention.mention_id, self.target.mention_id,)


class Sentiment(object):
    def __init__(self, source_mention, target_mention, polarity, sarcasm, src_auh_tag_p, src_auh_tag_q, tag_in_quote, src_in_quote, src_is_quoter, src_is_poster, src_tag_p, src_tag_q, tag_in_post, src_in_post, tag_is_quoter, tag_is_poster, tag_type, tag_mtype):
        self.source = source_mention
        self.target = target_mention # relation, event, or entity mention
        self.polarity = polarity
        self.sarcasm = sarcasm
        self.src_auh_tag_p = src_auh_tag_p #true if the source and the target its belief is toward to are in the same post
        self.src_auh_tag_q = src_auh_tag_q #true if the source and the target its belief is toward to are in the same quote
        self.tag_in_quote = tag_in_quote
        self.src_in_quote = src_in_quote
        self.src_is_quoter = src_is_quoter
        self.src_is_poster = src_is_poster
        self.src_tag_p = src_tag_p
        self.src_tag_q = src_tag_q
        self.tag_in_post = tag_in_post
        self.src_in_post = src_in_post
        self.tag_is_quoter = tag_is_quoter
        self.tag_is_poster = tag_is_poster
        self.tag_type = tag_type
        self.tag_mtype = tag_mtype

    def __repr__(self):
        return '<Sentiment {0} {1} {2}>'.format(self.polarity, self.source.mention_id, self.target.mention_id,)

class BeStAnnotations(object):
    """
    A set of Belief and Sentiment annotations linked against an EREAnnotation object.
    """
    def __init__(self, ere_annotations, src_range):
        self.sources = {}
        self.ere_annotations = ere_annotations
        self.beliefs = []
        self.sentiments = []
        self.argument_beliefs = []
        self.src_range = src_range #author_offsets

    def src_tag_flags(self, source_entity_mention, tag_offset):
        src_auh_tag_p = False
        src_auh_tag_q = False
        src_tag_p = False
        src_tag_q = False
        tag_in_quote = False
        src_in_quote = False
        tag_in_post = False
        src_in_post = False
        tag_is_quoter = False
        tag_is_poster = False
        src_is_quoter = False
        src_is_poster = False

        src_offset = source_entity_mention.offset
        author_offsets = self.src_range.author_offsets
        quoter_offsets = self.src_range.quote_begins
        post_ranges = self.src_range.post_ranges
        quote_ranges = self.src_range.quote_ranges

        if tag_offset in author_offsets:
            tag_is_poster = True
        if tag_offset in quoter_offsets:
            tag_is_quoter = True

        tag_range = set()
        src_range = set()
        for q_ranges in quote_ranges.values():
            for q_range in q_ranges:
                quote_begin = q_range[0]
                quote_end = q_range[1]
                if quote_begin < tag_offset < quote_end:
                    tag_in_quote = True
                    tag_range.update(q_range)
                if quote_begin < src_offset < quote_end:
                    src_in_quote = True
                    src_range.update(q_range)
                if tag_in_quote and src_in_quote:
                    break
        if tag_range == src_range and tag_range and src_in_quote:
            src_tag_q = True

        for p_ranges in post_ranges.values():
            for p_range in p_ranges:
                p_begin = p_range[0]
                p_end = p_range[1]
                if p_begin < tag_offset < p_end:
                    tag_in_post = True
                    tag_range.update(p_range)
                if p_begin < src_offset < p_end:
                    src_in_post = True
                    src_range.update(p_range)
                if tag_in_post and src_in_post:
                    break
        if tag_range == src_range and tag_range and src_in_post:
            src_tag_p = True

        if src_offset in quoter_offsets:
            src_is_quoter = True
            for quote_range in quote_ranges[src_offset]:
                quote_begin = quote_range[0]
                quote_end = quote_range[1]
                if quote_begin < tag_offset < quote_end:
                    src_auh_tag_q = True
                    src_tag_q = True
                    break

        if src_offset in author_offsets:
            src_is_poster = True
            for post_range in post_ranges[src_offset]:
                post_begin = post_range[0]
                post_end = post_range[1]
                if post_begin < tag_offset < post_end:
                    src_auh_tag_p = True
                    src_tag_p = True
                    break

        # ERROR ALTERS
        # if src_offset in author_offsets and src_offset in quoter_offsets:
        #     print '#########################ERROR ALTER######src_offset is both p/q#####################'
        #     print "src offset is: ", src_offset
        #     print 'tag_offset is: ', tag_offset
        #     print 'author_offsets is: ', author_offsets
        #     print 'quoter_offsets is: ', quoter_offsets
        #     print "post_ranges[src_offset] is: ", post_ranges[src_offset]
        #     print 'quote_ranges[src_offset] is: ', quote_ranges[src_offset]
        # if src_in_post and src_in_quote:
        #     print '#########################ERROR ALTER######src_offset is in both p/q####################'
        #     print "src offset is: ", src_offset
        #     print 'tag_offset is: ', tag_offset
        #     print 'author_offsets is: ', author_offsets
        #     print 'quoter_offsets is: ', quoter_offsets
        #     print 'quote_ranges is: ', quote_ranges
        #     print 'post_ranges is: ', post_ranges
        # if src_in_post and src_offset in author_offsets:
        #     print '#########################ERROR ALTER######src_in_post and src_offset in author_offsets############'
        #     print "src offset is: ", src_offset
        #     print 'tag_offset is: ', tag_offset
        #     print 'author_offsets is: ', author_offsets
        #     print 'quoter_offsets is: ', quoter_offsets
        #     print 'quote_ranges is: ', quote_ranges
        #     print 'post_ranges is: ', post_ranges
        # if src_in_post and src_offset in quoter_offsets:
        #     print '#########################ERROR ALTER######src_in_post and src_offset in quoter_offsets##############'
        #     print "src offset is: ", src_offset
        #     print 'tag_offset is: ', tag_offset
        #     print 'author_offsets is: ', author_offsets
        #     print 'quoter_offsets is: ', quoter_offsets
        #     print 'quote_ranges is: ', quote_ranges
        #     print 'post_ranges is: ', post_ranges
        # if src_in_quote and src_offset in author_offsets:
        #     print '#########################ERROR ALTER######src_in_quote and src_offset in author_offsets####################'
        #     print "src offset is: ", src_offset
        #     print 'tag_offset is: ', tag_offset
        #     print 'author_offsets is: ', author_offsets
        #     print 'quoter_offsets is: ', quoter_offsets
        #     print 'quote_ranges is: ', quote_ranges
        #     print 'post_ranges is: ', post_ranges
        # if src_in_quote and src_offset in quoter_offsets:
        #     print '#########################ERROR ALTER######src_in_quote and src_offset in quoter_offsets##################'
        #     print "src offset is: ", src_offset
        #     print 'tag_offset is: ', tag_offset
        #     print 'author_offsets is: ', author_offsets
        #     print 'quoter_offsets is: ', quoter_offsets
        #     print 'quote_ranges is: ', quote_ranges
        #     print 'post_ranges is: ', post_ranges

        return (src_auh_tag_p, src_auh_tag_q, tag_in_quote, src_in_quote, src_is_quoter, src_is_poster, src_tag_p, src_tag_q, tag_in_post, src_in_post, tag_is_quoter, tag_is_poster)

    def target_geter(self, mention):
        tag_offset = 0
        tag_type = 0
        tag_mtype = ''
        if isinstance(mention, RelationMention):
            tag_type = 1
            for rel in self.ere_annotations.relations.values():
                if mention in rel.mentions:
                    tag_mtype = rel.relation_type
            tag_offset = mention.rel_arg1.entity_mention.offset
        elif isinstance(mention, EventMention):
            tag_type = 0
            tag_mtype = mention.event_type
            tag_offset = mention.trigger.offset
        else:
            tag_type = 2
            for ent in self.ere_annotations.entities.values():
                if mention in ent.mentions:
                    tag_mtype = ent.entity_type
            tag_offset = mention.offset

        return (tag_offset, tag_type, tag_mtype)


    def integrate_beliefs_for_mention(self, etree, mention):
        """
        This method adds the annotated beliefs over a relation or event mention.
        """
        if etree is None:
            raise AssertionError('Got None, expected ElementTree object.')
        for child_et in etree:
            assert child_et.tag == 'belief'
            btype = child_et.get('type')
            polarity = child_et.get('polarity')
            sarcasm = child_et.get('sarcasm')
            # Get the source entity
            source_et = child_et.find('source')
            if source_et is None:
                sys.stderr.write('Warning: Skipping belief annotation for {0}. No source annotated.\n'.format(mention.mention_id))
            else:
                source_mention_id= source_et.get('id')
                if source_mention_id is None:
                    source_mention_id = source_et.get('ere_id')
                try:
                    source_entity_mention = self.ere_annotations.entity_mentions[source_mention_id]
                except KeyError:
                    raise AnnotationConsistencyError("Could not find entity mention {0} in ERE annotation.".format(source_mention_id))
                # These items are already recorded in the mention entry. No need to read them again.
                #source_et.text
                #source_et.get('offset')
                #source_et.get('length')

                tag_offset, tag_type, tag_mtype = self.target_geter(mention)
                src_auh_tag_p, src_auh_tag_q, tag_in_quote, src_in_quote, src_is_quoter, src_is_poster, src_tag_p, src_tag_q, tag_in_post, src_in_post, tag_is_quoter, tag_is_poster= self.src_tag_flags(source_entity_mention, tag_offset)
                belief = Belief(source_entity_mention, mention, btype, polarity, sarcasm, src_auh_tag_p, src_auh_tag_q, tag_in_quote, src_in_quote, src_is_quoter, src_is_poster, src_tag_p, src_tag_q, tag_in_post, src_in_post, tag_is_quoter, tag_is_poster, tag_type, tag_mtype)
                self.beliefs.append(belief)
                source_entity_mention.beliefs.append(belief)
                mention.beliefs.append(belief)

    def integrate_belief_relations(self, etree):
        for child_et in etree:
            if child_et.tag == 'relation':
                relation_mention_id = child_et.get('id')
                if relation_mention_id is None:
                    relation_mention_id = child_et.get('ere_id')
                try:
                    relation = self.ere_annotations.relation_mentions[relation_mention_id]
                except KeyError:
                    raise AnnotationConsistencyError("Could not find relation mention {0} in ERE annotation.".format(relation_mention_id))
                self.integrate_beliefs_for_mention(child_et.find('beliefs'), relation)
            else:
                pass # ignore triggers

    def integrate_beliefs_for_event_argument(self, etree, target_event_mention, argument):
        """
        This method adds the annotated beliefs over an event argument
        """

        for child_et in etree:
            assert child_et.tag == 'belief'
            btype = child_et.get('type')
            polarity = child_et.get('polarity')
            sarcasm = child_et.get('sarcasm')
            # Get the source entity
            source_et = child_et.find('source')
            if source_et is None:
                sys.stderr.write('Warning: Skipping belief annotation for argument {0} of {1}. No source annotated.\n'.format(argument.mention_id, target_event_mention.mention_id))
            else:
                source_mention_id= source_et.get('id')
                if source_mention_id is None:
                    source_mention_id = source_et.get('ere_id')
                try:
                    source_entity_mention = self.ere_annotations.entity_mentions[source_mention_id]
                except KeyError:
                    raise AnnotationConsistencyError("Could not find entity mention {0} in ERE annotation.".format(source_mention_id))

                tag_offset = 0
                if argument.is_filler:
                    tag_offset = argument.entity.offset
                else:
                    tag_offset = argument.entity_mention.offset

                src_auh_tag_p, src_auh_tag_q, tag_in_quote, src_in_quote, src_is_quoter, src_is_poster, src_tag_p, src_tag_q, tag_in_post, src_in_post, tag_is_quoter, tag_is_poster = self.src_tag_flags(source_entity_mention, tag_offset)

                belief = ArgumentBelief(source_entity_mention, target_event_mention, argument, btype, polarity, sarcasm, src_auh_tag_p, src_auh_tag_q, tag_in_quote, src_in_quote, src_is_quoter, src_is_poster, src_tag_p, src_tag_q, tag_in_post, src_in_post, tag_is_quoter, tag_is_poster)
                self.argument_beliefs.append(belief)
                source_entity_mention.argument_beliefs.append(belief)
                argument.beliefs.append(belief)

    def integrate_belief_event_arguments(self, etree, event):
        for child_et in etree:
            assert child_et.tag == 'arg'
            argument_mention_id = child_et.get('id')
            if argument_mention_id is None:
                argument_mention_id = child_et.get('ere_id')
            try:
                argument = event.arguments[argument_mention_id]
            except KeyError:
                raise AnnotationConsistencyError("Event mention {0} has no argument {1} in ERE annotation.".format(event.mention_id, argument_mention_id))
            for arg_child_et in child_et:
                if arg_child_et.tag == "beliefs":
                    self.integrate_beliefs_for_event_argument(arg_child_et, event, argument)
                elif arg_child_et.tag == "text":
                    pass # Already in ERE annotation
                else:
                    print tostring(child_et)
                    raise AnnotationFormatError('unexpected XML element {0}.'.format(tostring(arg_child_et)))

    def integrate_belief_events(self, etree):
        for child_et in etree:
            if child_et.tag == 'event':
                event_mention_id = child_et.get('id')
                if event_mention_id is None:
                    event_mention_id = child_et.get('ere_id')
                try:
                    event = self.ere_annotations.event_mentions[event_mention_id]
                except KeyError:
                    raise AnnotationConsistencyError("Could not find event mention {0} in ERE annotation.".format(event_mention_id))
                for event_child_et in child_et:
                    if event_child_et.tag == "beliefs":
                        self.integrate_beliefs_for_mention(event_child_et, event)
                    elif event_child_et.tag == "arguments":
                        self.integrate_belief_event_arguments(event_child_et, event)
                    elif event_child_et.tag =="trigger":
                        pass
                    else:
                        raise AnnotationFormatError('unexpected XML element {0}.'.format(tostring(event_child_et)))

            else:
                pass # ignore triggers

    def integrate_belief_annotations(self, etree, source):
        for child_et in etree:
            if child_et.tag == 'relations':
                self.integrate_belief_relations(child_et)
            elif child_et.tag == 'events':
                self.integrate_belief_events(child_et)
            else:
                raise AnnotationFormatError('unexpected XML element {0}.'.format(tostring(child_et)))

    def integrate_sentiments_for_mention(self, etree, mention):
        """
        This method adds the annotated beliefs over a relation or event mention.
        """
        if etree is None:
            raise AssertionError('Got None, expected ElementTree object.')
        for child_et in etree:
            if child_et.tag == 'sentiment':
                polarity = child_et.get('polarity')
                sarcasm = child_et.get('sarcasm')
                # Get the source entity
                source_et = child_et.find('source')
                if source_et is None:
                    # sys.stderr.write('Warning: Skipping sentiment annotation for {0}. No source annotated.\n'.format(mention.mention_id))
                else:
                    source_mention_id= source_et.get('id')
                    if source_mention_id is None:
                        source_mention_id = source_et.get('ere_id')
                    try:
                        source_entity_mention = self.ere_annotations.entity_mentions[source_mention_id]
                    except KeyError:
                        raise AnnotationConsistencyError("Could not find entity mention {0} in ERE annotation.".format(source_mention_id))
                    # These items are already recorded in the mention entry. No need to read them again.
                    #source_et.text
                    #source_et.get('offset')
                    #source_et.get('length')
                    tag_offset, tag_type, tag_mtype = self.target_geter(mention)
                    src_auh_tag_p, src_auh_tag_q, tag_in_quote, src_in_quote, src_is_quoter, src_is_poster, src_tag_p, src_tag_q, tag_in_post, src_in_post, tag_is_quoter, tag_is_poster = self.src_tag_flags(source_entity_mention, tag_offset)

                    sentiment = Sentiment(source_entity_mention, mention, polarity, sarcasm, src_auh_tag_p, src_auh_tag_q, tag_in_quote, src_in_quote, src_is_quoter, src_is_poster, src_tag_p, src_tag_q, tag_in_post, src_in_post, tag_is_quoter, tag_is_poster, tag_type, tag_mtype)
                    self.sentiments.append(sentiment)
                    source_entity_mention.has_sentiments.append(sentiment)
                    mention.sentiments.append(sentiment)

    def integrate_sentiment_relations(self, etree):
        for child_et in etree:
            if child_et.tag == 'relation':
                relation_mention_id = child_et.get('id')
                if relation_mention_id is None:
                    relation_mention_id = child_et.get('ere_id')
                try:
                    relation = self.ere_annotations.relation_mentions[relation_mention_id]
                except KeyError:
                    raise AnnotationConsistencyError("Could not find relation mention {0} in ERE annotation.".format(relation_mention_id))
                self.integrate_sentiments_for_mention(child_et.find('sentiments'), relation)
            else:
                pass # ignore triggers

    def integrate_sentiment_events(self, etree):
        for child_et in etree:
            if child_et.tag == 'event':
                event_mention_id = child_et.get('id')
                if event_mention_id is None:
                    event_mention_id = child_et.get('ere_id')
                try:
                    event = self.ere_annotations.event_mentions[event_mention_id]
                except KeyError:
                    raise AnnotationConsistencyError("Could not find event mention {0} in ERE annotation.".format(event_mention_id))
                self.integrate_sentiments_for_mention(child_et.find('sentiments'), event)
            else:
                pass # ignore triggers

    def integrate_sentiment_entities(self, etree):
        for child_et in etree:
            if child_et.tag == 'entity':
                entity_mention_id = child_et.get('id')
                if entity_mention_id is None:
                    entity_mention_id = child_et.get('ere_id')
                try:
                    entity = self.ere_annotations.entity_mentions[entity_mention_id]
                except KeyError:
                    raise AnnotationConsistencyError("Could not find entity mention {0} in ERE annotation.".format(entity_mention_id))
                self.integrate_sentiments_for_mention(child_et.find('sentiments'), entity)
            else:
                pass # ignore triggers

    def integrate_sentiment_annotations(self, etree, source):
        for child_et in etree:
            if child_et.tag == 'entities':
                self.integrate_sentiment_entities(child_et)
            elif child_et.tag == 'relations':
                self.integrate_sentiment_relations(child_et)
            elif child_et.tag == 'events':
                self.integrate_sentiment_events(child_et)
            else:
                raise AnnotationFormatError('unexpected XML element {0}.'.format(tostring(child_et)))

    def integrate_etree(self, etree):
        """
        Add the content of an e-tree object into the annotation data structure.
        """
        root = etree.getroot()
        # Create a new source object
        source = BeStDocumentSource(root.get('id'))
        if not source.doc_id in self.sources:
            self.sources[source.doc_id] = source
        else:
            source = self.sources[source.doc_id]

        # Now process the children
        for child in root:
            if child.tag == 'belief_annotations':
                self.integrate_belief_annotations(child, source)
            elif child.tag == 'sentiment_annotations':
                self.integrate_sentiment_annotations(child, source)
            else:
                raise AnnotationFormatError('unexpected XML element {0}.'.format(tostring(child_et)))


class BeStDocumentSource(object):
    def __init__(self, doc_id):
        self.doc_id = doc_id

class SrcRange(object):
    '''
    Represent a set of index ranges of author_offsets, quote_begins, quote_ranges and post_ranges
    '''
    def __init__(self):
        self.author_offsets = []
        self.quote_begins = []
        self.post_ranges = {}
        self.quote_ranges = {}

    def build_ranges(self, content):
        author_offsets = [m.end() for m in re.finditer('post author="', content)]
        self.author_offsets = author_offsets
        post_ends = [m.end() for m in re.finditer('</post>', content)]
        quote_begins = [m.end() for m in re.finditer('<quote orig_author="', content)]
        special_cases = [m.end() for m in re.finditer('<quote>', content)]
        quote_begins.extend(special_cases) #uncomment if '<quote>' is not the same as author
        quote_begins = sorted(quote_begins)
        self.quote_begins = quote_begins
        quote_ends = [m.end() for m in re.finditer('</quote>', content)]
        #comment if '<quote>' is not the same as author
        # for sp_ind in special_cases:
        #     for q_end in quote_ends:
        #         if q_end > sp_ind:
        #             quote_ends.remove(q_end)
        #             break
        # print 'author_offsets is {0}, post_ends is {1}'.format(author_offsets, post_ends)
        # print 'quote_begins is {0}, quote_ends is {1}'.format(quote_begins, quote_ends)

        # nested_q_begins = []
        quote_ranges = {q_begin : [] for q_begin in quote_begins}
        quote_outer_ranges =[]

        q_ind = 0
        q_begin_stack = []
        q_end = []
        if quote_begins:
            cur_q_begin = quote_begins[q_ind]
            most_inner_continue_flag = False
            for q_end in quote_ends:
                # print 'q_end this time is {0}---------------------'.format(q_end)
                most_inner = False
                cur_end_ind = quote_ends.index(q_end)
                # print 'cur_q_begin is: ', cur_q_begin
                while cur_q_begin < q_end:
                    if q_ind >= len(quote_begins):
                        break
                    most_inner = True
                    q_begin_stack.append(cur_q_begin)
                    # print 'in while loop q_begin_stack is {0}: '.format(q_begin_stack)
                    if len(q_begin_stack) >= 2:
                        last_q_begin = q_begin_stack[len(q_begin_stack)-2]
                        if most_inner_continue_flag:
                            last_q_end = quote_ends[cur_end_ind - 1]
                            quote_ranges[last_q_begin].append([last_q_end, cur_q_begin])
                            # print 'most_inner_continue_flag is true: quote_ranges of {0} adds {1}'.format(last_q_begin, [last_q_end, cur_q_begin])
                            most_inner_continue_flag = False
                        else:
                            quote_ranges[last_q_begin].append([last_q_begin, cur_q_begin])
                            # print 'quote_ranges of {0} adds {1}'.format(last_q_begin, [last_q_begin, cur_q_begin])

                    q_ind += 1
                    if q_ind < len(quote_begins):
                        cur_q_begin = quote_begins[q_ind]

                stack_top = q_begin_stack.pop()
                # print 'stack_top is popped: ', stack_top

                if cur_end_ind + 2 <= len(quote_ends) and q_begin_stack:
                    next_q_end = quote_ends[cur_end_ind + 1]
                    if q_end < cur_q_begin < next_q_end:
                        # print "get most_inner_continue_flag here"
                        most_inner_continue_flag = True

                cur_top = 0
                if most_inner:
                    cur_top = stack_top
                else:
                    cur_top = quote_ends[cur_end_ind - 1]

                quote_ranges[stack_top].append([cur_top, q_end])
                # print "quote_ranges of {0} adds {1}".format(stack_top, [cur_top, q_end])

                if not q_begin_stack:
                    quote_outer_ranges.append([stack_top, q_end])
                    # print "quote_outer_ranges adds: ",[stack_top, q_end]
                    # print "quote_outer_ranges now is: ", quote_outer_ranges


        self.quote_ranges = quote_ranges
        # print 'quote_ranges is: ', quote_ranges.items()

        post_ranges = {}
        for i in xrange(len(author_offsets)):
            author_offset = author_offsets[i]
            post_end = post_ends[i]
            post_range = []
            if quote_outer_ranges:
                quote_outer_ranges = sorted(quote_outer_ranges)
                range_top = None
                range_bom = None
                for j in xrange(len(quote_outer_ranges)):
                    if range_top:
                        if author_offset <= quote_outer_ranges[j][0] <= range_top[0]:
                            range_top = quote_outer_ranges[j]
                    else:
                        if author_offset <= quote_outer_ranges[j][0]:
                            range_top = quote_outer_ranges[j]
                    if range_bom:
                        if range_bom[1] <= quote_outer_ranges[j][1] <= post_end:
                            range_bom = quote_outer_ranges[j]
                    else:
                        if quote_outer_ranges[j][1] <= post_end:
                            range_bom = quote_outer_ranges[j]

                quotes_in_post = []
                if range_top and range_bom:
                    quotes_in_post = quote_outer_ranges[quote_outer_ranges.index(range_top):quote_outer_ranges.index(range_bom)+1]
                    quote_outer_ranges = [x for x in quote_outer_ranges if x not in quotes_in_post]

                if quotes_in_post:
                    post_range.append([author_offset, quotes_in_post[0][0]])
                    if len(quotes_in_post) == 1:
                        post_range.append([quotes_in_post[0][1], post_end])
                    else:
                        post_range.append([quotes_in_post[0][1], quotes_in_post[1][0]])
                        for q in xrange(1, len(quotes_in_post)):
                            if q + 1 < len(quotes_in_post):
                                post_range.append([quotes_in_post[q][1], quotes_in_post[q+1][0]])
                            else:
                                post_range.append([quotes_in_post[q][1], post_end])
                    post_ranges[author_offset] = post_range

            if author_offset not in post_ranges.keys() or not post_ranges[author_offset]:
                post_range.append([author_offset, post_end])
                post_ranges[author_offset] = post_range

        self.post_ranges = post_ranges
        # print 'post_ranges is: ', post_ranges.items()

def read_source(source_path):
    src_range = SrcRange()
    with open(str(source_path)) as f:
        content = ''
        for line in f:
            line = unicode(line, "utf-8")
            content += line
        src_range.build_ranges(content)

    return src_range

def read_best_xml(ere_annotations, src_range, *sources):
    """
    Read in one or more XML sources and return a BeStAnnotations bject.
    """
    annotations = BeStAnnotations(ere_annotations, src_range); # The new annotations object

    for source_f in sources:
        et = ET.parse(source_f)
        annotations.integrate_etree(et)

    return annotations


if __name__ == '__main__': # Test the XML readers
    if len(sys.argv) == 4:
        ere_annotations = read_ere_xml(sys.argv[1])
        print('Successfully read ERE annotations from {0}.'.format(sys.argv[1]))
        src_range = read_source(sys.argv[3])
        best_annotations = read_best_xml(ere_annotations, src_range, sys.argv[2])
        print('Successfully read BeSt annotations from {0}.'.format(sys.argv[2]))
    else:
        print('deft_best.py -- library for DEFT ERE and BeSt annotations.')
        print(' This script should terminate without error if the annotations are well formed.')
        print(' Usage: python deft_best.py [rich ere XML file] [BeST XML file]')
