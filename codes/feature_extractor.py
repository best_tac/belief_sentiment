#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
This module takes source, ere, and best files, extracts sentences, features, and
annotations to produce a json file with all info for training and testing.
'''
import sys
import re
import os
import json
import codecs
import pickle
import pandas as pd
from xml.parsers import expat
import xml.etree.ElementTree as ET
from os.path import isfile, isdir, join, splitext, split, exists
from get_ranges_offsets import SrcRange, get_sent_begins
from nltk import ne_chunk, pos_tag, word_tokenize, tokenize
from nltk.parse.stanford import StanfordDependencyParser, StanfordParser

#set up StanfordParser dirs
path_to_jar = '/Users/taoyds/Documents/workspace/stanford-corenlp-full-2015-12-09/stanford-corenlp-3.6.0.jar'
path_to_models_jar = '/Users/taoyds/Documents/workspace/stanford-corenlp-full-2015-12-09/stanford-corenlp-3.6.0-models.jar'
parser = StanfordParser(path_to_jar=path_to_jar, path_to_models_jar=path_to_models_jar)
stanford_tokenizer = tokenize.StanfordTokenizer(path_to_jar=path_to_jar)

def usage():
    print ("python feature_extractor.py <input dir>")
    print ("<input dir> is the dir where contains source, ere, and annotation folders.")


def remove_tags(text):
    '''
    remove URLs and other special symbols
    '''
    text = re.sub('<[^>]*>', '', text)
    return re.sub('/', ' ', text)


def mid_offerset_dicts_getter(root):
    '''
    get dicts of entity mention to its offset and relation mention to its arg1's offset
    used to spot offset of targets of relations in best files
    '''
    offset_emid = {int(em.get('offset')) : em.get('id') for em in root.iter('entity_mention')}
    emid_offset = {em.get('id') : int(em.get('offset')) for em in root.iter('entity_mention')}
    emid_endoffset = {em.get('id') : int(em.get('offset'))+ int(em.get('length')) for em in root.iter('entity_mention')} #int(em.get('offset') + em.get('length'))
    relm_offset = {relm.get('id') : emid_offset[relm.find('rel_arg1').get('entity_mention_id')] for relm in root.iter('relation_mention')}

    return (offset_emid, emid_offset, emid_endoffset, relm_offset)


def sent_pq_newline_index(sent_range, pq_ranges, newline_ranges):
    '''
    get post/quote and paragraph indexs where the sentence is located at
    '''
    sent_range1, sent_range2 = sent_range[0], sent_range[1]
    pq_ind = 0
    newline_ind = 0
    for pq_offset, pq_rangs in pq_ranges.items():
        for pq_range in pq_rangs:
            pq_range1, pq_range2 = pq_range[0], pq_range[1]
            if pq_range1 < sent_range1 and sent_range2 < pq_range2:
                pq_ind = pq_offset
                break
    for newline_offset, newline_range in newline_ranges.items():
        newline_range1, newline_range2 = newline_range[0], newline_range[1]
        if newline_range1 < sent_range1 and sent_range2 < newline_range2:
            newline_ind = newline_offset
            break
    return (pq_ind, newline_ind)


def get_sent_ranges(source_path):
    '''
    get all ranges of authors, new added 'The author says', posts and quotes
    '''
    if isfile(source_path):
        src_ranges = SrcRange()
        with open(str(source_path)) as f:
            content = ''
            for line in f:
                line = unicode(line, "utf-8")
                content += line

            src_ranges.build_ranges(content)
            # print 'content with whitespace is: ', repr(content)
            # author_say_inds = src_ranges.author_inds
            author_say_inds = get_sent_begins(source_path)
            # print 'author_say_inds is: ', author_say_inds
            src_ranges.author_inds = author_say_inds
            post_ranges = src_ranges.post_ranges
            quote_ranges = src_ranges.quote_ranges
            pq_ranges = post_ranges.copy()
            pq_ranges.update(quote_ranges)
            newline_inds = [m.start() for m in re.finditer('\n', content)]
            newline_ranges = {i : (newline_ind, newline_inds[i+1]) for i, newline_ind in enumerate(newline_inds) if i+1 < len(newline_inds)}
            sent_ranges = [(author_say_ind, author_say_inds[i+1]) for i, author_say_ind in enumerate(author_say_inds) if i+1 < len(author_say_inds)]
            last_range = (author_say_inds[len(author_say_inds)-1], len(content))
            sent_ranges.append(last_range)
            # print 'author_say_inds is: ', author_say_inds
            # print 'sent_ranges is: ', sent_ranges
            range_sent = {}
            for sent_range in sent_ranges:
                sent_beg = sent_range[0]
                sent_end = sent_range[1]
                sent_text = content[sent_beg:sent_end]
                if '\n' in sent_text:
                    # print 'sent_range is: ', sent_range
                    # print 'sent_text before split is: ', sent_text
                    splited_sent = sent_text.split('\n')
                    sent_text = splited_sent[0]
                    # print 'splited_sent is: ', splited_sent
                    # print 'sent_text after is: ', sent_text
                    for newline_ind in newline_inds:
                        if sent_beg < newline_ind:
                            sp_range = (sent_beg, newline_ind)
                            pq_ind, newline_ind = sent_pq_newline_index(sp_range, pq_ranges, newline_ranges)
                            range_sent[sp_range] = (sent_text, pq_ind, newline_ind)
                            break
                else:
                    pq_ind, newline_ind = sent_pq_newline_index(sent_range, pq_ranges, newline_ranges)
                    range_sent[sent_range] = (sent_text, pq_ind, newline_ind)
            #debug
            for rang, sent in range_sent.items():
                if rang[0] > rang[1]:
                    print 'rang[0] is: ', rang[0]
                    print 'rang[1] is: ', rang[1]
                    print 'range_sent.items() is: ', range_sent.items()
                    print 'range_sent error \n'
                elif sent[0] is None:
                    print 'range_sent error - sent \n'

            return (range_sent, content, src_ranges)


def get_sentiments(best_path):
    '''
    get all sentiments in best files
    '''
    if isfile(best_path):
        best_tree = ET.parse(best_path)
        best_root = best_tree.getroot()

        sentiment_annotations = best_root.find('sentiment_annotations')
        elem_names = ['entity', 'relation', 'event']
        sentiments = []
        tagids_sarcasm_sentiment = []
        if sentiment_annotations is not None:
            for elem_name in elem_names:
                for elem_sb in sentiment_annotations.iter(elem_name):
                    tag_id = elem_sb.get('ere_id')
                    sentiments_tree = elem_sb.find('sentiments')
                    for child_et in sentiments_tree.iter('sentiment'):
                        src_id = None
                        src_offset = None
                        src_text = None
                        polarity = child_et.get('polarity')
                        sarcasm = child_et.get('sarcasm')
                        if polarity == 'pos' or polarity == 'neg' or sarcasm == 'yes':
                            tagids_sarcasm_sentiment.append(tag_id)

                        source = child_et.find('source')
                        if source is not None:
                            src_id = source.get('ere_id')
                            src_offset = int(source.get('offset'))
                            src_text = source.text
                        sentiment = (tag_id, src_id, src_offset, polarity, src_text, sarcasm)
                        sentiments.append(sentiment)
        return sentiments, tagids_sarcasm_sentiment


def get_beliefs(best_path):
    '''
    get all beliefs in best files
    '''
    if isfile(best_path):
        best_tree = ET.parse(best_path)
        best_root = best_tree.getroot()
        belief_annotations = best_root.find('belief_annotations')
        elem_names = ['relation', 'event']
        beliefs = []
        if belief_annotations is not None:
            for elem_name in elem_names:
                for elem_sb in belief_annotations.iter(elem_name):
                    tag_id = elem_sb.get('ere_id')
                    belief_elems = elem_sb.find('beliefs')
                    # print 'belief_elems is: ', belief_elems
                    for belief_elem in belief_elems.iter('belief'):
                        btype = belief_elem.get('type')
                        source = belief_elem.find('source')
                        sarcasm = belief_elem.get('sarcasm')
                        src_id = None
                        src_offset = None
                        src_text = None
                        if source is not None:
                            src_id = source.get('ere_id')
                            src_offset = int(source.get('offset'))
                            src_text = source.text
                        belief = (tag_id, src_id, src_offset, btype, src_text, sarcasm)
                        beliefs.append(belief)
        return beliefs


def get_smaller_sentences_ranges(sentence):
    m_t = []
    sp_ranges = []
    small_s_ranges = []
    try:
        t = parser.raw_parse(sentence)
        t = list(t)[0]
        trees = []
        for s in t.subtrees(lambda t: t.label() == 'S'):
            trees.append(s)

        # if len(trees) > 0:
        #     m_t = trees[0].leaves()
        # else:
        m_t = t.leaves()

        if len(trees) > 1:
            small_trees = []
            for i in xrange(len(trees)):
                if i == 0:
                    continue
                r = i + 1
                ex_flag = True
                cur_tw = set(trees[i].leaves())
                while r < len(trees):
                    r_tw = set(trees[r].leaves())
                    r += 1
                    if cur_tw > r_tw:
                        ex_flag = False

                if ex_flag:
                    small_trees.append(trees[i].leaves())

            cur_mt = 0
            is_match = [False]
            for sm_t in small_trees:
                sm_t_r = cur_mt
                # print sm_t
                matches = []
                continuous = 0
                start_ind = 0
                for w in sm_t:
                    while cur_mt < len(m_t) and w != m_t[cur_mt]:
                        is_match.append(False)
                        if is_match[len(is_match) - 2] and continuous != len(sm_t):
                            for m in matches:
                                while cur_mt < len(m_t) and m != m_t[cur_mt]:
                                    cur_mt += 1
                                    is_match.append(False)
                                cur_mt += 1
                                if not is_match[len(is_match)-1]:
                                    start_ind = cur_mt
                                is_match.append(True)
                        else:
                            cur_mt += 1
                            continuous = 0

                    continuous += 1
                    matches.append(w)
                    cur_mt += 1
                    if not is_match[len(is_match)-1]:
                        start_ind = cur_mt
                    if continuous == len(sm_t):
                        small_s_ranges.append([start_ind, cur_mt])
                    is_match.append(True)

            for i, sm_range in enumerate(small_s_ranges):
                if i == 0:
                    mid_range = [1, sm_range[0]-1]
                else:
                    last_range = small_s_ranges[i-1]
                    mid_range = [last_range[1]+1, sm_range[0]-1]

                if not mid_range[0] > mid_range[1]:
                    sp_ranges.append(mid_range)
                sp_ranges.append(sm_range)

                if i == len(small_s_ranges) - 1:
                    mid_range = [sm_range[1]+1, len(m_t)]
                    if not mid_range[0] > mid_range[1]:
                        sp_ranges.append(mid_range)
        else:
            s_range = [1, len(m_t)]
            sp_ranges.append(s_range)

    except Exception:
        pass

    return sp_ranges, m_t, small_s_ranges


def create_dataset(source_path, best_path, ere_path, fname, missed):
    '''
    create a train/test dataset in json format from original source and xml files
    '''
    range_sent, content, src_ranges = get_sent_ranges(source_path)
    sentiments, tagids_sarcasm_sentiment = get_sentiments(best_path)
    missed_id_pairs = missed[fname]
    missed_dict = {}
    for t, s in missed_id_pairs:
        if t not in missed_dict.keys():
            missed_dict[t] = [s]
        else:
            missed_dict[t].append(s)
    beliefs = get_beliefs(best_path)
    author_offsets = src_ranges.author_offsets
    quote_begins = src_ranges.quote_begins
    author_offsets.extend(quote_begins)
    author_inds = src_ranges.author_inds
    post_ranges = src_ranges.post_ranges
    quote_ranges = src_ranges.quote_ranges
    pq_ranges = post_ranges.copy()
    pq_ranges.update(quote_ranges)
    tag_sentences = []
    tags_in_sent = {rang : {} for rang in range_sent.keys()}
    if isfile(ere_path):
        ere_tree = ET.parse(ere_path)
        ere_root = ere_tree.getroot()
        parent_map = {c:p for p in ere_root.iter() for c in p}
        parent_id_map = {c.get('id'):p.get('id') for c, p in parent_map.items() if c.tag == 'entity_mention'}
        offset_emid, emid_offset, emid_endoffset, relm_offset = mid_offerset_dicts_getter(ere_tree)
        fid_offset = {f.get('id') : int(f.get('offset')) for f in ere_tree.iter('filler')}
        fid_endoffset = {f.get('id') : int(f.get('offset'))+int(f.get('length')) for f in ere_tree.iter('filler')}

        tag_ms = ['entity_mention', 'relation_mention', 'event_mention']
        author_entities = []
        for entm in ere_root.iter('entity_mention'):
            entm_offset = int(entm.get('offset'))
            if entm_offset in author_offsets:
                ent = parent_map[entm]
                author_entities.append(ent)
        author_entities = list(set(author_entities))
        author_entity_ids = [ent.get('id') for ent in author_entities]

        #get all entity_mention ids in each sentence
        for emid, offset in emid_offset.items():
            for sent_range in range_sent.keys():
                if sent_range[0] <= offset < sent_range[1]:
                    ent_id = parent_id_map[emid]
                    if ent_id not in tags_in_sent[sent_range].keys():
                        tags_in_sent[sent_range][ent_id] = [(emid, emid_offset[emid])]
                    else:
                        tags_in_sent[sent_range][ent_id].append((emid, emid_offset[emid]))
                    break

        for tag_m in tag_ms:
            for target_mention in ere_root.iter(tag_m):
                tag_id = target_mention.get('id')
                if tag_id not in missed_dict.keys():
                    continue
                # else:
                #     print 'trying to add tag_id {0} -------\n'.format(tag_id)
                parent = parent_map[target_mention]
                trigger_offest = None
                trigger_text = None
                reg_offsets = []
                tag_range = []
                tag_offset = None
                target_ind = None
                target_ranges = []
                not_target_inds = []
                words_in_target_ranges = []
                tag_type = None
                sentence = ''
                tag_text = ''
                p_id = None
                p_type = ''
                bf_tag_text = ''
                af_tag_text = ''
                src_tag_dis = None
                pq_ind = None
                newline_ind = None
                btype = 'none'
                src_text_bf = None
                src_offset_bf = None
                src_is_author_bf = 1
                src_id_bf = None
                sarcasm_bf = 'none'
                src_offset = None
                polarity = 'neutral'
                sarcasm = 'none'
                src_text = None
                src_id = None
                src_is_author = 1
                sentence_w_tags = ''
                small_sentence_w_tag = ''
                all_emids = None
                p_emoffsets = {}
                p_emwinds = {}
                not_target_inds_v2 = []
                target_inds_v2 = []
                wps = {}
                not_target_ranges_v2 = []
                is_target_author = 0
                #debug
                change_flag = False

                if tag_m == 'entity_mention':
                    tag_offset = int(target_mention.get('offset'))
                    mention_text = target_mention.find('mention_text')
                    try:
                        tag_text = mention_text.text
                    except:
                        print 'Warning: the entity_mention with tag_offset {0} does not have mention_text'.format(tag_offset)
                    #skip author's entity mentions
                    if parent in author_entities:
                        is_target_author = 1
                        my_words = ['I', 'i', 'my', 'My']
                        if tag_text in my_words:
                            if tag_id not in tagids_sarcasm_sentiment:
                                continue
                            else:
                                print 'tag_id {0} is an author but has a sentiment or sarcasm towards it!-----------'.format(tag_id)
                    if tag_offset in author_offsets:
                        continue

                    p_id = parent.get('id')
                    p_type = 'entity'
                    mtype = parent.get('type')
                    tag_len = int(target_mention.get('length'))
                    subtype = target_mention.get('noun_type')
                    tag_range = [tag_offset, tag_offset+tag_len]

                    #debug
                    if tag_range[0] >= tag_range[1]:
                        print 'em range error ----',tag_range[0],tag_range[1]
                    reg_offsets.extend(tag_range)
                elif tag_m == 'relation_mention':
                    p_id = parent.get('id')
                    p_type = 'relation'
                    mtype = parent.get('type')
                    subtype = parent.get('subtype')
                    trigger = target_mention.find('trigger')
                    if trigger is not None:
                        trigger_offest = int(trigger.get('offset'))
                        trigger_endoffest = trigger_offest+int(trigger.get('length'))
                        trigger_text = trigger.text
                        reg_offsets = [trigger_offest, trigger_endoffest]

                    reg1 = target_mention.find('rel_arg1')
                    reg2 = target_mention.find('rel_arg2')
                    if reg1 is not None:
                        reg1_emid = reg1.get('entity_mention_id')
                        reg1_offset = emid_offset[reg1_emid]
                        reg1_endoffset = emid_endoffset[reg1_emid]
                        reg_offsets.append(reg1_offset)
                        reg_offsets.append(reg1_endoffset)
                    if reg2 is not None:
                        reg2_emid = reg2.get('entity_mention_id')
                        if reg2_emid is not None:
                            reg2_offset = emid_offset[reg2_emid]
                            reg2_endoffset = emid_endoffset[reg2_emid]
                            reg_offsets.append(reg2_offset)
                            reg_offsets.append(reg2_endoffset)
                    reg_offsets = sorted(reg_offsets)
                    tag_range = [reg_offsets[0], reg_offsets[len(reg_offsets)-1]]
                    if trigger is not None:
                        tag_offset = trigger_offest
                    else:
                        tag_offset = tag_range[0]

                    #debug
                    if tag_range[0] >= tag_range[1]:
                        print 'relm range error ----',tag_range[0],tag_range[1]
                    elif tag_range[1] > tag_range[0] + 100:
                        change_flag = True
                        print 'relm range too big ----', tag_range[0],tag_range[1]

                elif tag_m == 'event_mention':
                    p_id = parent.get('id')
                    p_type = 'event'
                    trigger = target_mention.find('trigger')
                    if trigger is not None:
                        trigger_offest = int(trigger.get('offset'))
                        trigger_endoffest = trigger_offest+int(trigger.get('length'))
                        trigger_text = trigger.text
                        reg_offsets = [trigger_offest, trigger_endoffest]
                    mtype = target_mention.get('type')
                    subtype = target_mention.get('subtype')
                    regs = target_mention.findall('em_arg')
                    for reg in regs:
                        arg_mid = reg.get('entity_mention_id')
                        if arg_mid is not None:
                            arg_offset = emid_offset[arg_mid]
                            arg_endoffset = emid_endoffset[arg_mid]
                            reg_offsets.append(arg_offset)
                            reg_offsets.append(arg_endoffset)
                        else:
                            arg_mid = reg.get('filler_id')
                            if arg_mid is not None:
                                arg_offset = fid_offset[arg_mid]
                                arg_endoffset = fid_endoffset[arg_mid]
                                reg_offsets.append(arg_offset)
                                reg_offsets.append(arg_endoffset)

                    reg_offsets = sorted(reg_offsets)
                    tag_range = [reg_offsets[0], reg_offsets[len(reg_offsets)-1]]
                    if trigger is not None:
                        tag_offset = trigger_offest
                    else:
                        tag_offset = tag_range[0]

                    #debug
                    if tag_range[0] >= tag_range[1]:
                        print 'eventm range error ----',tag_range[0],tag_range[1]
                    elif tag_range[1] > tag_range[0] + 200:
                        change_flag = True
                        print 'eventm range too big ----', tag_range[0],tag_range[1]



                if change_flag:
                    print 'fixing range too big issue-------------\n'
                    l = len(reg_offsets) - 1
                    while tag_range[1] > tag_range[0] + 100 and l > 0:
                        l -= 1
                        tag_range[1] = reg_offsets[l]
                    tag_offset = tag_range[0]
                    reg_offsets = reg_offsets[:l+1]

                for s_range, sent in range_sent.items():
                    s_range_l = int(s_range[0])
                    s_range_r = int(s_range[1])
                    if s_range_l <= tag_offset < s_range_r:
                        for srcoffset, ranges in pq_ranges.items():
                            for rg in ranges:
                                if rg[0] <= s_range_l and s_range_r <= rg[1]:
                                    src_offset = int(srcoffset)
                                    if src_offset in offset_emid.keys():
                                        src_id = offset_emid[src_offset]
                                        src_endoffset = emid_endoffset[src_id]
                                        src_text = content[src_offset:src_endoffset]
                                        src_text_bf = src_text
                                        src_id_bf = src_id
                                        src_offset_bf = src_offset
                        tag_range1 = s_range_l
                        tag_range2 = s_range_r
                        for reg_offset in reg_offsets:
                            if s_range_l <= reg_offset:
                                tag_range1 = reg_offset
                                break
                        for reg_offset in sorted(reg_offsets, reverse=True):
                            if s_range_r >= reg_offset:
                                tag_range2 = reg_offset
                                break
                        tag_range = [tag_range1, tag_range2]
                        if tag_text == '':
                            tag_text = content[tag_range1:tag_range2]
                        # sent_content = sent[0]
                        sentence = remove_tags(content[s_range_l:s_range_r])
                        pq_ind = sent[1]
                        newline_ind = sent[2]
                        bf_tag_text = remove_tags(content[s_range_l:tag_range[0]])
                        af_tag_text = remove_tags(content[tag_range[1]:s_range_r])
                        src_tag_dis = len(bf_tag_text)
                        emid_offsets = []
                        #to get the dict of ent_id : corresponding em indexs for coreference
                        for ent_id, emids in tags_in_sent[s_range].items():
                            for emid in emids:
                                e_offset = emid[1]
                                emid_offsets.append(e_offset)
                                if ent_id not in p_emoffsets.keys():
                                    p_emoffsets[ent_id] = [e_offset]
                                else:
                                    p_emoffsets[ent_id].append(e_offset)

                        if p_id not in p_emoffsets.keys():
                            p_emoffsets[p_id] = [tag_offset]
                            emid_offsets.append(tag_offset)
                        # emid_offsets.append(s_range[0]+16)
                        emid_offsets.append(s_range_r)
                        emid_offsets.append(tag_offset)
                        emid_offsets = list(set(emid_offsets))
                        emid_offsets = sorted(emid_offsets)
                        # print 'emid_offsets is: ', emid_offsets
                        emid_offsets_len = len(emid_offsets)
                        for i in xrange(emid_offsets_len):
                            if i == 0:
                                # print 'now emid_offsets[i] is: ', emid_offsets[i]
                                sentence_w_tags = remove_tags(content[s_range_l:emid_offsets[i]])
                            elif i < emid_offsets_len:
                                # print 'now emid_offsets[i] is: ', emid_offsets[i]
                                sentence_w_tags += ' MENTION_INDICATOR' + remove_tags(content[emid_offsets[i-1]:emid_offsets[i]]).strip()


                        words = stanford_tokenizer.tokenize(sentence_w_tags)
                        # print 'words is: ', words
                        mw_inds = [i+1 for i, w in enumerate(words) if 'MENTION_INDICATOR' in w]
                        mw_counts = [w.count('MENTION_INDICATOR') for i, w in enumerate(words) if 'MENTION_INDICATOR' in w]
                        for i in xrange(len(mw_inds)):
                            count = mw_counts[i]
                            while count > 1:
                                mw_inds.append(mw_inds[i])
                                count -= 1
                        mw_inds = sorted(mw_inds)
                        # print 'mw_inds is: ', mw_inds
                        flated_inds = [i for inds in p_emoffsets.values() for i in inds]
                        flated_inds = sorted(list(set(flated_inds)))
                        ind_ranks = {ind: i for i, ind in enumerate(flated_inds)}
                        # print 'flated_inds is: ', flated_inds
                        # print 'mw_inds is: ', mw_inds
                        # print 'tag_text is: ', tag_text
                        if len(flated_inds) == len(mw_inds):
                            offset_inds = zip(flated_inds, mw_inds)
                            target_ind = [offset_ind[1] for offset_ind in offset_inds if offset_ind[0] == tag_offset]
                            try:
                                target_ind = target_ind[0]
                                # print 'target_ind is: ',target_ind
                            except:
                                print 'sentence is: ', sentence
                                print 'tag_text is: ',target_ind
                                print 'target_ind can not be found!------------\n'
                            not_target_inds = [ind for ind in mw_inds if ind != target_ind]
                            if not_target_inds == None:
                                not_target_inds = []
                            # print 'not_target_inds after removing target_ind: ', not_target_inds
                            for p, emoffsets in p_emoffsets.items():
                                token_inds = []
                                for emoffset in emoffsets:
                                    token_ind = [offset_ind[1] for offset_ind in offset_inds if offset_ind[0] == emoffset]
                                    token_inds.append(token_ind[0])
                                p_emwinds[p] = token_inds
                        else:
                            print 'Annotation index error, the sentence is ignored.'
                            print 'tag_offset is: ', tag_offset
                            print 'words is: ', words
                            print 'flated_inds is: ', flated_inds
                            print 'mw_inds is: ', mw_inds
                            print 'tag_text is: ', tag_text
                            print '---------------------------\n'
                            if tag_id not in tagids_sarcasm_sentiment:
                                print 'len(flated_inds) != len(mw_inds) -------------'
                                sentence = ''
                            else:
                                print 'adding an error case because tag_id {0} has a sentiment or sarcasm towards it!-----------'.format(tag_id)

                        all_emids = tags_in_sent[s_range]
                        break

                #debug
                # print p_emoffsets
                if sentence == '':
                    print 'range_sent keys are: ', range_sent.keys()
                    print "the tag_offset {0} is not in any sentences and sentence is empty------ \n".format(tag_offset)
                    continue
                if tag_text == '' and sentence != '':
                    print 'the tag text is empty: ', tag_offset, tag_id

                if sentence != '' or tag_id in tagids_sarcasm_sentiment:
                    # print 'target text is: ', tag_text
                    for p, ew in p_emwinds.items():
                        if p != p_id:
                            not_target_inds_v2.extend(ew)
                        else:
                            target_inds_v2 = ew

                    tokenized_words = stanford_tokenizer.tokenize(sentence)
                    # print 'tokenized_words is: ',tokenized_words
                    for i, word in enumerate(tokenized_words):
                        if word not in wps.keys():
                            wps[word] = [(word, i+1)]
                        else:
                            wps[word].append((word, i+1))

                    if len(tokenized_words) > 150:
                        print 'tokenized_words is bigger than 150-------------------\n'
                        t_start = target_ind-8 if target_ind-8 >= 0 else 0
                        t_end = target_ind+8 if target_ind+8 < len(tokenized_words) else len(tokenized_words)-1
                        new_sw = tokenized_words[t_start:t_end]
                        new_s = ''
                        target_ind = 9
                        for w in new_sw:
                            new_s += ' ' + w
                        sentence = new_s
                        tokenized_words = stanford_tokenizer.tokenize(sentence)
                        for i, word in enumerate(tokenized_words):
                            if word not in wps.keys():
                                wps[word] = [(word, i+1)]
                            else:
                                wps[word].append((word, i+1))

                    # print 'sentence after is: ', sentence
                    splited_s_ranges, m_t, small_s_ranges = get_smaller_sentences_ranges(sentence)
                    # print 'splited_s_ranges is: ', splited_s_ranges
                    if len(splited_s_ranges) == 0 and len(m_t) == 0:
                        continue

                    if len(tokenized_words) != len(m_t):
                        print 'stanford parser len {0} is not the same as stanford_tokenizer.tokenize len {1} ----'.format(len(m_t), len(tokenized_words))
                        print 'stanford_tokenizer.tokenize is: ', tokenized_words
                        print 'stanford tree is: {0}---\n'.format(m_t)

                    target_range = []
                    for sp_range in splited_s_ranges:
                        sp_range_l = sp_range[0]
                        sp_range_r = sp_range[1]
                        if sp_range_l <= target_ind <= sp_range_r:
                            target_range = sp_range

                    # print 'target_ind is: ', target_ind
                    # print 'sentence is: ', sentence
                    # print 'len of words is: ', len(tokenized_words)
                    # print 'splited_s_ranges is: ', splited_s_ranges
                    if len(target_range) != 0:
                        target_range_ind = splited_s_ranges.index(target_range)
                    else:
                        rg_len = len(splited_s_ranges)
                        target_range_ind = rg_len-2
                        try:
                            l = rg_len
                            tag_firstw = tag_text.split()[0]
                            while l > 0:
                                l -= 1
                                target_range = splited_s_ranges[l]
                                tag_range_words = tokenized_words[target_range[0]-1:target_range[1]]
                                i = 0
                                for w in tag_range_words:
                                    i += 1
                                    if w == tag_firstw:
                                        target_ind = target_range[0] + i
                                        target_range_ind = l
                                        break
                        except:
                            print 'sentence can not find any target_range: ', sentence
                            print 'target_ind is: ', target_ind
                            print 'target_text is: ', tag_text
                            print 'target_range is: ', target_range
                            print 'splited_s_ranges is {0} ------\n'.format(splited_s_ranges)
                            # continue

                    smaller_sent_range_inds = [target_range_ind-2,target_range_ind-1,target_range_ind, target_range_ind+1, target_range_ind+2]
                    for i in smaller_sent_range_inds:
                        if 0 <= i < len(splited_s_ranges):
                            pos_trange = splited_s_ranges[i]
                            pos_range_l = pos_trange[0]
                            pos_range_r = pos_trange[1]
                            add_flag = True
                            for nt_ind in not_target_inds:
                                if pos_range_l <= nt_ind <= pos_range_r \
                                and (pos_trange in small_s_ranges or pos_range_r-pos_range_l > 10)\
                                and not (pos_range_l <= target_ind <= pos_range_r):
                                    add_flag = False
                                    break
                            if add_flag:
                                target_ranges.append(pos_trange)
                    # print 'target_ranges is ', target_ranges
                    #get sentiment info
                    sentiment_sources = []
                    for sentiment in sentiments:
                        if tag_id == sentiment[0]:
                            polarity = sentiment[3]
                            sarcasm = sentiment[5]
                            if sentiment[1] != None:
                                src_text = sentiment[4]
                                src_id = sentiment[1]
                                src_offset = sentiment[2]
                                if src_offset not in author_offsets:
                                    print '#------------sentiment case when src is not the author----------------#'
                            if src_id in missed_dict[tag_id]:
                                sentiment_sources.append((src_id, src_offset, src_text, polarity, sarcasm))
                            if src_offset not in author_offsets:
                                src_is_author = 0


                    #get belief info
                    for belief in beliefs:
                        if tag_id == belief[0]:
                            btype = belief[3]
                            sarcasm_bf = belief[5]
                            if belief[1] != None:
                                src_text_bf = belief[4]
                                src_id_bf = belief[1]
                                src_offset_bf = belief[2]
                            if src_offset_bf not in author_offsets:
                                src_is_author_bf = 0
                            break
                    # print 'src_text belief is: ', src_text_bf
                    # print 'belief type is: ', btype
                    #get small_sentence_w_tag
                    for i, tk_w in enumerate(tokenized_words):
                        for t_range in target_ranges:
                            if t_range[0] <= i+1 <= t_range[1]:
                                small_sentence_w_tag += ' ' + tk_w

                    for t_range in target_ranges:
                        ind_r = t_range[0]-1
                        ind_l = t_range[1]
                        words_in_target_range = []
                        for i in range(ind_r, ind_l):
                            if i < len(tokenized_words):
                                words_in_target_range.append(tokenized_words[i])
                        words_in_target_ranges.append(words_in_target_range)
                    # print 'words_in_target_ranges is: ', words_in_target_ranges
                    # bf_tag_words = stanford_tokenizer.tokenize(bf_tag_text)
                    # af_tag_words = stanford_tokenizer.tokenize(af_tag_text)
                    # tag_words_num = len(stanford_tokenizer.tokenize(tag_text))
                    # bf_tag_rdis = len(bf_tag_words) / float(len(tokenized_words)) if len(tokenized_words) > 0 else 0.0
                    # af_tag_rdis = len(af_tag_words) / float(len(tokenized_words)) if len(tokenized_words) > 0 else 0.0

                    #debug

                    # print 'small_sentence_w_target is: ', small_sentence_w_tag
                    # print '*********************************\n'

                    for s in sentiment_sources:
                        src_id = s[0]
                        src_offset = s[1]
                        src_text = s[2]
                        polarity = s[3]
                        sarcasm = s[4]
                        print 'now adding tag_id {0} and src_id {1} -------------\n'.format(tag_id, src_id)
                        tag_sentence = {'file_name': fname, 'original_sentence': sentence, 'tokenized_sentence': tokenized_words,\
                                        'sentence_targets_indicator': sentence_w_tags, 'small_sentence_w_target': small_sentence_w_tag,\
                                        'target_id': tag_id, 'target_text': tag_text, 'target_index': target_ind, 'target_offset': tag_offset,\
                                        'source_offset_sentiment': src_offset, 'source_is_author_sentiment': src_is_author,\
                                        'source_text_sentiment': src_text, 'source_id_sentiment': src_id,\
                                        'source_text_belief': src_text_bf, 'source_offset_belief': src_offset_bf,\
                                        'source_is_author_belief': src_is_author_bf, 'source_id_belief': src_id_bf,\
                                        'parent_id': p_id, 'parent_type': p_type, 'mtype': mtype, 'subtype': subtype,\
                                        'polarity': polarity, 'sarcasm_sentiment': sarcasm,\
                                        'belief_type': btype, 'sarcasm_belief': sarcasm_bf,\
                                        'entid_emoffsets': p_emoffsets, 'words_in_target_ranges': words_in_target_ranges,\
                                        'splited_s_ranges': splited_s_ranges, 'target_ranges': target_ranges,\
                                        'wps': wps, 'target_inds_v2': target_inds_v2, 'not_target_inds': not_target_inds,\
                                        'p_emwinds': p_emwinds, 'target_is_author_mention': is_target_author}

                        tag_sentences.append(tag_sentence)
                    # del missed_dict[tag_id]

        # for i in missed_dict.keys():
        #     print 'target with {0} sentiment or sarcasm is still not inclueded !!!!!!!!!!!!!!!!!!!!!'.format(i)
    return tag_sentences

def transfrom_all_indir(input_dir):
    """
    process all source, ere, and annotation files in the input dir, and write all new source,
    ere, and annotation files to source_new, ere_new, and annotation_new folders in this dir.
    """
    if not isdir(input_dir):
        usage()
    else:
        # sys.stdout = open("sentiment_na_examples.txt", "w")
        src_input_dir = os.path.join(input_dir,"source")
        ere_input_dir = os.path.join(input_dir,"ere")
        best_input_dir = os.path.join(input_dir,"annotation")
        json_output = os.path.join(input_dir,"engdf_data_v2.json")
        total_tag_sentences = []
        total_neutral_sentences = []
        total_pos_sentences = []
        total_neg_sentences = []
        # filenames = [f for f in os.listdir(src_input_dir) if f.endswith('.cmp.txt')]
        filenames = [f for f in os.listdir(ere_input_dir) if f.startswith('ENG_DF')]
        for f in filenames:
            # base_name = f.replace('.cmp.txt','')
            # print "processing file is: {0} \n".format(base_name)
            # ere_fn = '{0}.rich_ere.xml'.format(base_name)
            # best_fn = '{0}.best.xml'.format(base_name)
            # #input files
            # src_path = os.path.join(src_input_dir, f)
            # ere_path = os.path.join(ere_input_dir, ere_fn)
            # best_path = os.path.join(best_input_dir, best_fn)

            base_name = f.replace('.rich_ere.xml','')
            f_name = f[:32]
            print "processing file is: {0} \n".format(base_name)
            src_fn = '{0}.xml'.format(f_name)
            best_fn = '{0}.best.xml'.format(base_name)

            src_path = os.path.join(src_input_dir, src_fn)
            ere_path = os.path.join(ere_input_dir, f)
            best_path = os.path.join(best_input_dir, best_fn)

            #if ere and best files with the same name as source file exist
            if isfile(src_path) and isfile(best_path):
                with open('/Users/taoyds/Desktop/file_missed.txt', 'rb') as handle:
                    missed = pickle.loads(handle.read())
                tag_sentences = create_dataset(src_path, best_path, ere_path, base_name, missed)
                total_tag_sentences.extend(tag_sentences)

                neutral_sentences = []
                pos_sentences = []
                neg_sentences = []
                [neutral_sentences.append(m) if m['polarity'] == 'neutral' or m['polarity'] == 'none' else pos_sentences.append(m) for m in tag_sentences]
                total_neutral_sentences.extend(neutral_sentences)
                total_pos_sentences.extend(pos_sentences)
                total_neg_sentences.extend(neg_sentences)


        print 'there are total {0} sentences.\n'.format(len(total_tag_sentences))
        print 'there are total {0} neutral_sentences and its per is {1} \n'.format(len(total_neutral_sentences), len(total_neutral_sentences)/float(len(total_tag_sentences)))
        print 'there are total {0} pos/neg_sentences and its per is {1} \n'.format(len(total_pos_sentences), len(total_pos_sentences)/float(len(total_tag_sentences)))

        # write new json file
        with codecs.open(json_output, 'w', 'utf8') as f:
            f.write(json.dumps(total_tag_sentences, sort_keys = True, ensure_ascii=False))

        print('Successfully write the new json file to {0}.'.format(input_dir))


if __name__ == '__main__':
    if len(sys.argv) == 2:
        transfrom_all_indir(sys.argv[1])
