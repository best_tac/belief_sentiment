#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
This module takes source, ere, and best files, adds 'The author says ' to the beginning
of each sentence in source, and update offsets, entity mentions and sources in ere and
best files. It produces new source, ere, and best files for input data.
'''
import sys
import re
import os
import random
import xml.etree.ElementTree as ET
from nltk import ne_chunk, pos_tag, word_tokenize, tokenize
from nltk.tree import Tree
from xml.etree.ElementTree import tostring
from xml.etree.ElementTree import Element
from xml.etree.ElementTree import SubElement
from os import listdir, makedirs
from os.path import isfile, isdir, join, splitext, split, exists

__author__ = 'Tao Yu'
__email__ = 'ty2326@columbia.edu'
__date__ = 'June 15 2016'

def usage():
    print ("python add_author_says.py <input dir>")
    print ("<input dir> is the dir where contains source, ere, and annotation folders.")


def get_chunks(text):
    chunked = ne_chunk(pos_tag(word_tokenize(text)))
    prev = None
    current_chunk = []
    for i in chunked:
        if type(i) == Tree:
            current_chunk.append([token for token, pos in i.leaves()])
    return current_chunk

def transform_source(src_path):
    '''
    Add 'The author says ' to the beginning of each detected sentence in each source file
    Store the indexs of the beginning of each sentence in the original source file

    Because we are going to change the original offsets in ere and best files
    only based on the number of 'The author says ' added and the positions of them,
    it is crucial to not add or delete any space (even empty space) but only 'The author says '
    '''
    src_original = open(src_path, 'r')
    author_say = 'The author says '
    sent_num = 0
    src_content = ''
    with src_original as f:
        for line in f:
            line = unicode(line, "utf-8")
            if (not line.startswith('<') and line != '\n') or line.startswith('<a href'): #if the line is a real text
                line_add_author = ''
                sent_start_inds = []
                for sent in tokenize.sent_tokenize(line): #sent_tokenize will trimed out empty spaces bw sentences
                    sent_num += 1
                    sent_start_ind = line.find(sent)
                    sent_start_inds.append(sent_start_ind)

                sent_start_inds[0] = 0
                for ind in xrange(len(sent_start_inds)):
                    sent_start_ind = sent_start_inds[ind]
                    if ind + 1 == len(sent_start_inds):
                        sent = line[sent_start_ind:]
                    else:
                        sent_end_ind = sent_start_inds[ind + 1]
                        sent = line[sent_start_ind: sent_end_ind]
                    ne_chunks = [word for ne_chunk in get_chunks(sent) for word in ne_chunk]
                    # print 'ne_chunks is: ', ne_chunks
                    first_word = word_tokenize(sent)[0] if word_tokenize(sent) else None
                    # print 'first_word is: ', first_word
                    # print first_word in ne_chunks
                    if len(sent) > 0 and first_word not in ne_chunks and first_word != 'I':
                        sent = sent[0].lower() + sent[1:] #TODO: use ne_chunk
                    line_add_author += author_say + sent
                src_content += line_add_author
            else:
                src_content += line

    return src_content


class SrcRange(object):
    '''
    Represent a set of index ranges of author_offsets, quote_begins, quote_ranges and post_ranges
    '''
    def __init__(self):
        self.author_offsets = []
        self.quote_begins = []
        self.post_ranges = {}
        self.quote_ranges = {}
        self.author_inds = [] #the indexs of added 'The author says ' in the new source
        self.author_inds_original = [] #the indexs of added 'The author says ' in the original source
        self.offset_change_ranges = {} #a dict stores what number old offsets should add to get new offsets

    def build_ranges(self, content):
        #find the indexs of added 'The author says ' in the new source file
        author_say = 'The author says '
        author_inds = [m.start() for m in re.finditer(author_say, content)]
        #find the indexs of added 'The author says ' in the original source file based on author_inds above
        #and offset_change_ranges is a dict whose
        #key: the number should be added to the old offset in order to get the new offset;
        #value: the range of old offsets that should add the number
        add_num = 0
        author_inds_original = []
        offset_change_ranges = {}
        pre_ind = 0
        for i in author_inds:
            offset_movedback = add_num * len(author_say)
            ind = i - offset_movedback
            author_inds_original.append(ind)
            if len(author_inds_original) > 1:
                pre_ind = author_inds_original[add_num-1]
            offset_change_ranges[offset_movedback] = [pre_ind, ind]
            add_num += 1

        self.author_inds = author_inds
        self.author_inds_original = author_inds_original
        self.offset_change_ranges = offset_change_ranges

        author_offsets = [m.end() for m in re.finditer('post author="', content)]
        self.author_offsets = author_offsets
        post_ends = [m.end() for m in re.finditer('</post>', content)]
        quote_begins = [m.end() for m in re.finditer('<quote orig_author="', content)]
        special_cases = [m.end() for m in re.finditer('<quote>', content)]
        # quote_begins.extend(special_case)
        quote_begins = sorted(quote_begins)
        self.quote_begins = quote_begins
        quote_ends = [m.end() for m in re.finditer('</quote>', content)]

        for sp_ind in special_cases:
            for q_end in quote_ends:
                if q_end > sp_ind:
                    quote_ends.remove(q_end)
                    break

        #build ranges of quotes
        quote_ranges = {q_begin : [] for q_begin in quote_begins}
        quote_outer_ranges =[]
        q_ind = 0
        q_begin_stack = []
        q_end = []
        if quote_begins:
            cur_q_begin = quote_begins[q_ind]
            most_inner_continue_flag = False
            for q_end in quote_ends:
                most_inner = False
                cur_end_ind = quote_ends.index(q_end)
                while cur_q_begin < q_end:
                    if q_ind >= len(quote_begins):
                        break
                    most_inner = True
                    q_begin_stack.append(cur_q_begin)
                    if len(q_begin_stack) >= 2:
                        last_q_begin = q_begin_stack[len(q_begin_stack)-2]
                        if most_inner_continue_flag:
                            last_q_end = quote_ends[cur_end_ind - 1]
                            quote_ranges[last_q_begin].append([last_q_end, cur_q_begin])
                            most_inner_continue_flag = False
                        else:
                            quote_ranges[last_q_begin].append([last_q_begin, cur_q_begin])
                    q_ind += 1
                    if q_ind < len(quote_begins):
                        cur_q_begin = quote_begins[q_ind]
                stack_top = q_begin_stack.pop()

                if cur_end_ind + 2 <= len(quote_ends) and q_begin_stack:
                    next_q_end = quote_ends[cur_end_ind + 1]
                    if q_end < cur_q_begin < next_q_end:
                        most_inner_continue_flag = True

                cur_top = 0
                if most_inner:
                    cur_top = stack_top
                else:
                    cur_top = quote_ends[cur_end_ind - 1]

                quote_ranges[stack_top].append([cur_top, q_end])
                if not q_begin_stack:
                    quote_outer_ranges.append([stack_top, q_end])
        self.quote_ranges = quote_ranges

        #build the ranges of posts based on quote_ranges
        post_ranges = {}
        for i in xrange(len(author_offsets)):
            author_offset = author_offsets[i]
            post_end = post_ends[i]
            post_range = []
            if quote_outer_ranges:
                quote_outer_ranges = sorted(quote_outer_ranges)
                range_top = None
                range_bom = None
                for j in xrange(len(quote_outer_ranges)):
                    if range_top:
                        if author_offset <= quote_outer_ranges[j][0] <= range_top[0]:
                            range_top = quote_outer_ranges[j]
                    else:
                        if author_offset <= quote_outer_ranges[j][0]:
                            range_top = quote_outer_ranges[j]
                    if range_bom:
                        if range_bom[1] <= quote_outer_ranges[j][1] <= post_end:
                            range_bom = quote_outer_ranges[j]
                    else:
                        if quote_outer_ranges[j][1] <= post_end:
                            range_bom = quote_outer_ranges[j]

                quotes_in_post = []
                if range_top and range_bom:
                    quotes_in_post = quote_outer_ranges[quote_outer_ranges.index(range_top):quote_outer_ranges.index(range_bom)+1]
                    quote_outer_ranges = [x for x in quote_outer_ranges if x not in quotes_in_post]

                if quotes_in_post:
                    post_range.append([author_offset, quotes_in_post[0][0]])
                    if len(quotes_in_post) == 1:
                        post_range.append([quotes_in_post[0][1], post_end])
                    else:
                        post_range.append([quotes_in_post[0][1], quotes_in_post[1][0]])
                        for q in xrange(1, len(quotes_in_post)):
                            if q + 1 < len(quotes_in_post):
                                post_range.append([quotes_in_post[q][1], quotes_in_post[q+1][0]])
                            else:
                                post_range.append([quotes_in_post[q][1], post_end])
                    post_ranges[author_offset] = post_range

            if author_offset not in post_ranges.keys() or not post_ranges[author_offset]:
                post_range.append([author_offset, post_end])
                post_ranges[author_offset] = post_range

        self.post_ranges = post_ranges


def get_src_ranges(source_path):
    '''
    get all ranges of authors, new added 'The author says', posts and quotes
    '''
    src_ranges = SrcRange()
    with open(str(source_path)) as f:
        content = ''
        for line in f:
            line = unicode(line, "utf-8")
            content += line
        src_ranges.build_ranges(content)
    return src_ranges


def update_all_offset(root, elems_have_offset, offset_change_ranges, flag):
    '''
    iter over each element with offset attrib to update its offset (when flag is True)
    or set the offset back to the original one (when flag is False) based on offset_change_ranges
    '''
    for elem_name in elems_have_offset:
        for element in root.iter(elem_name):
            if not element.get('offset') is None:
                old_offset = int(element.get('offset'))
                new_offset = old_offset
                for offset_added, offset_change_range in offset_change_ranges.items():
                    if flag: #update its offset (when flag is True)
                        left = offset_change_range[0]
                        right = offset_change_range[1]
                        if left <= old_offset < right: #check if the old offset in one of the ranges
                            new_offset = old_offset + offset_added #then add corresponding offset_movedback to get new offset
                            break
                    else: #set the offset back to the original one (when flag is Flase)
                        left = offset_change_range[0] + offset_added
                        right = offset_change_range[1] + offset_added
                        if left <= old_offset < right:
                            new_offset = old_offset - offset_added
                            break

                element.set('offset', str(new_offset))


def add_em_author(root, author_offsets, author_inds, ranges):
    '''
    build author entity mentions for the author of a post or quote
    '''
    parent_map = {c:p for p in root.iter() for c in p} #ere_tree.iter()
    for entity_mention in root.iter('entity_mention'):
        offset = int(entity_mention.get('offset'))
        if offset in author_offsets: #if the element is an author
            for pq_range in ranges[offset]: #for current post or quote ranges of the real author
                pq_begin = pq_range[0]
                pq_end = pq_range[1]
                for author_ind in author_inds: #for each offset of "The author says"
                    if pq_begin < author_ind < pq_end: #see if it in any post/quote range
                        parent = parent_map[entity_mention]
                        id_num = random.randint(10000, 99999)
                        em_id = 'm-' + str(id_num)
                        noun_type = entity_mention.get('noun_type')
                        source = entity_mention.get('source')
                        offset_added = str(author_ind)
                        length = str(10)
                        #if it is, then add it to em and its parent (entity) is the same as the real author
                        entity_mention_added = SubElement(parent, "entity_mention", id=em_id,
                                                    noun_type=noun_type, source=source,
                                                    offset=offset_added, length=length)
                        mention_text = SubElement(entity_mention_added, 'mention_text')
                        mention_text.text = 'The author'


def transform_ere(ere_path, src_ranges):
    '''
    Add new added 'The author' entity mentions to the entities of authors
    '''
    ere_tree = ET.parse(ere_path)
    ere_root = ere_tree.getroot()

    author_offsets = src_ranges.author_offsets
    quote_begins = src_ranges.quote_begins
    author_offsets.extend(quote_begins)

    post_ranges = src_ranges.post_ranges
    quote_ranges = src_ranges.quote_ranges
    ranges = post_ranges.copy()
    ranges.update(quote_ranges)

    author_inds = src_ranges.author_inds
    offset_change_ranges = src_ranges.offset_change_ranges

    #the elements whose attributes have offset
    ere_elems_have_offset = ['nom_head', 'entity_mention', 'filler', 'trigger']
    update_all_offset(ere_root, ere_elems_have_offset, offset_change_ranges, True)
    #start to add new added 'The author' entity_mention to ere
    add_em_author(ere_root, author_offsets, author_inds, ranges)

    return ere_root


def relm_offset_getter(root):
    '''
    get dicts of entity mention to its offset and relation mention to its arg1's offset
    used to spot offset of targets of relations in best files
    '''
    offset_emid = {int(em.get('offset')) : em.get('id') for em in root.iter('entity_mention')}
    emid_offset = {em.get('id') : int(em.get('offset')) for em in root.iter('entity_mention')}
    emid_endoffset = {em.get('id') : int(em.get('offset'))+ int(em.get('length')) for em in root.iter('entity_mention')} #int(em.get('offset') + em.get('length'))
    relm_offset = {relm.get('id') : emid_offset[relm.find('rel_arg1').get('entity_mention_id')] for relm in root.iter('relation_mention')}

    return (offset_emid, emid_offset, emid_endoffset, relm_offset)


def transform_pred_best(best_root, ere_root, src_ranges):
    '''
    set src text in best.xml file back to original name of the poster or the quoter
    '''
    pred_best_root = best_root

    author_offsets = src_ranges.author_offsets
    quote_begins = src_ranges.quote_begins
    author_offsets.extend(quote_begins)

    post_ranges = src_ranges.post_ranges
    quote_ranges = src_ranges.quote_ranges
    ranges = post_ranges.copy()
    ranges.update(quote_ranges)

    offset_change_ranges = src_ranges.offset_change_ranges

    #get a dict of author and its attributes from ere file
    author_attrib = {}
    for entity_mention in ere_root.iter('entity_mention'):
        em_offset = int(entity_mention.get('offset'))
        if em_offset in author_offsets:
            author_id = entity_mention.get('id')
            author_length = entity_mention.get('length')
            mention_name = entity_mention.find('mention_text').text
            author_attrib[em_offset] = (author_id, author_length, mention_name)

    #find new added 'The author' sources, and then use ranges to determine its real author offset,
    #and utilize the author_attrib to set the source back to original source
    for source in pred_best_root.iter('source'):
        if source.text == 'The author':
            src_offset = int(source.get('offset'))
            for pq_offset, pq_ranges in ranges.items():
                for pq_range in pq_ranges:
                    range_left = pq_range[0]
                    range_right = pq_range[1]
                    if range_left < src_offset < range_right:
                        try:
                            author_attribs = author_attrib[pq_offset]
                            org_src_id = author_attribs[0]
                            org_src_length = author_attribs[1]
                            org_src_text = author_attribs[2]
                            source.set('ere_id', org_src_id)
                            source.set('offset', str(pq_offset))
                            source.set('length', org_src_length)
                            source.text = org_src_text
                            break
                        except KeyError:
                            raise AuthorConsistencyError("Could not find author attributes with offset {0} in ere file.".format(pq_offset))

    #finally update all offset in the best file
    best_elems_have_offset = ['trigger', 'source', 'arg', 'entity']
    update_all_offset(pred_best_root, best_elems_have_offset, offset_change_ranges, False)

    return pred_best_root


def update_author_src(element, offset_emid, relm_offset, author_inds, author_offsets, is_belief):
    '''
    change the source that is an author of the element to 'the author' entity mention added above
    '''
    if is_belief:
        beliefs = element.find('beliefs')
        all_belief = beliefs.findall('belief')
        sources = []
        for belief in all_belief:
            source = belief.find('source')
            if source is not None:
                sources.append(source)
    else:
        sentiment = element.find('sentiment')
        sources = sentiment.findall('source')

    if sources: #check some element does not have source child
        for source in sources:
            src_offset = int(source.get('offset'))
            tag_offset = 0
            if src_offset in author_offsets:
                if element.tag == 'event':
                    trigger = element.find('trigger')
                    tag_offset = int(trigger.get('offset'))
                elif element.tag == 'relation':
                    rel_id = element.get('ere_id')
                    tag_offset = relm_offset[rel_id]
                elif element.tag == 'entity' or element.tag == 'arg':
                    tag_offset = int(element.get('offset'))
                #set attribs of the author source
                for ind in sorted(author_inds, reverse=True):
                    if tag_offset > ind and ind in offset_emid.keys(): #if set for <quote> ?
                        author_id = offset_emid[ind]
                        source.set('ere_id', author_id)
                        source.set('offset', str(ind))
                        source.set('length', str(10))
                        source.text = 'The author'
                        break


def update_all_author_src(root, elems_have_source, offset_emid, relm_offset, author_inds, author_offsets):
    '''
    update all author entity mention for all element in the best file
    '''
    for elem_name in elems_have_source:
        if elem_name == 'entity':
            for elem in root.iter(elem_name):
                update_author_src(elem, offset_emid, relm_offset, author_inds, author_offsets, False)
        elif elem_name == 'arg':
            for elem in root.iter(elem_name):
                update_author_src(elem, offset_emid, relm_offset, author_inds, author_offsets, True)
        else:
            belief_annotations = root.find('belief_annotations')
            for elem_belief in belief_annotations.iter(elem_name):
                update_author_src(elem_belief, offset_emid, relm_offset, author_inds, author_offsets, True)
            sentiment_annotations = root.find('sentiment_annotations')
            for elem_sentiment in sentiment_annotations.iter(elem_name):
                update_author_src(elem_sentiment, offset_emid, relm_offset, author_inds, author_offsets, False)


def transform_best(best_path, src_ranges, ere_root):
    '''
    update author sources to new added 'The author' in the best file
    '''
    best_tree = ET.parse(best_path)
    best_root = best_tree.getroot()
    #get new 'the author' indexs in source and offset_change_ranges
    author_inds = src_ranges.author_inds
    offset_change_ranges = src_ranges.offset_change_ranges

    author_offsets = src_ranges.author_offsets
    quote_begins = src_ranges.quote_begins
    author_offsets.extend(quote_begins)

    #first update all offset in the best file
    best_elems_have_offset = ['trigger', 'source', 'arg', 'entity']
    update_all_offset(best_root, best_elems_have_offset, offset_change_ranges, True)
    #get offset of targets of relations from the ere file
    offset_emid, emid_offset, emid_endoffset, relm_offset = relm_offset_getter(ere_root)

    #call update_all_author_src for all element which has source to check
    elems_have_source = ['entity', 'arg', 'relation', 'event']
    update_all_author_src(best_root, elems_have_source, offset_emid, relm_offset, author_inds, author_offsets)

    return best_root


def transfrom_all_indir(input_dir):
    """
    process all source, ere, and annotation files in the input dir, and write all new source,
    ere, and annotation files to source_new, ere_new, and annotation_new folders in this dir.
    """
    if not isdir(input_dir):
        usage()
    else:
        src_input_dir = os.path.join(input_dir,"source")
        ere_input_dir = os.path.join(input_dir,"ere")
        best_input_dir = os.path.join(input_dir,"annotation")

        src_output_dir = os.path.join(input_dir,"source_original")
        ere_output_dir = os.path.join(input_dir,"ere_original")
        best_output_dir = os.path.join(input_dir,"annotation_original")
        predicted_best_dir = os.path.join(input_dir,"predicted_best")

        #if dirs do not exist, makedirs
        if not exists(src_output_dir):
            makedirs(src_output_dir)
        if not exists(ere_output_dir):
            makedirs(ere_output_dir)
        if not exists(best_output_dir):
            makedirs(best_output_dir)
        if not exists(predicted_best_dir):
            makedirs(predicted_best_dir)

        filenames = [f for f in os.listdir(src_input_dir) if f.endswith('.cmp.txt')]
        for f in filenames:
            base_name = f.replace('.cmp.txt','')
            print "processing file is: ", base_name
            ere_fn = '{0}.rich_ere.xml'.format(base_name)
            best_fn = '{0}.best.xml'.format(base_name)
            #input files
            src_path = os.path.join(src_input_dir, f)
            ere_path = os.path.join(ere_input_dir, ere_fn)
            best_path = os.path.join(best_input_dir, best_fn)
            #output files
            src_new_path = os.path.join(src_output_dir, f)
            ere_new_path = os.path.join(ere_output_dir, ere_fn)
            best_new_path = os.path.join(best_output_dir, best_fn)
            predicted_best_path = os.path.join(predicted_best_dir, best_fn)

            #if ere and best files with the same name as source file exist
            if isfile(ere_path) and isfile(best_path):
                # src_old = get_src_ranges(src_path)
                #transform source files
                src_content = transform_source(src_path)
                #write new source file
                src_new_file = open(src_new_path, 'w')
                src_new_file.write(src_content.encode('utf8'))
                # src_new_file.write(src_content.encode('utf8'))
                src_new_file.close()

                #get new source ranges of authors, posts and quotes
                src_range_new = get_src_ranges(src_new_path)

                #transform the ere file
                ere_root = transform_ere(ere_path, src_range_new)
                #write new ere file
                ere_new_file = open(ere_new_path, 'w')
                ere_new_file.write(ET.tostring(ere_root, encoding="UTF-8" ))
                ere_new_file.close()

                #transform the best file
                best_root = transform_best(best_path, src_range_new, ere_root)
                #write new best file
                best_new_file = open(best_new_path, 'w')
                best_new_file.write(ET.tostring(best_root, encoding="UTF-8" ))
                best_new_file.close()

                #transform the new best files back to original best files as the final predicted best files
                pred_best_root = transform_pred_best(best_root, ere_root, src_range_new)
                #write predicted best file
                best_new_file = open(predicted_best_path, 'w')
                best_new_file.write(ET.tostring(pred_best_root, encoding="UTF-8" ))
                best_new_file.close()

        print('Successfully write new source, ere and annotation files to {0}.'.format(input_dir))


if __name__ == '__main__':
    if len(sys.argv) == 2:
        transfrom_all_indir(sys.argv[1])
